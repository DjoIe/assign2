						--id, credit card,		nickname,				address,						,phone       person
insert into PROFILES values (1, '0489653252156487', 'Emouyouesee' , '2/14 Mickey Road, Mouse Town 2184', '0458913462',1);
insert into PROFILES values (2, '1234567890123456', 'konya', '3 Jolly Road, Horse Town 2114', '0452314462', 2);
						--id, firstName, lastName, 		email,			role, username, password, activated
insert into PEOPLE values (1, 'Topolino', 'Chiazza', 'this@legit.email', 3, 'mickey', 'disney', true);
insert into PEOPLE values (2, 'Bullseye', 'Dzolli', 'totasdlly@legit.email',3, 'tally', 'tom', true);
--managers
		--Monte Carlo
insert into PEOPLE values (5, 'Hurewq', 'Gibberish','to@legit.email', 2, 'Qwerty', 'reallyreallyreallylong',true);
insert into PEOPLE values (6, 'Gianlucca', 'Ney', 'tlly@legit.email', 2, 'Gianni', 'RomaLazio',true);
insert into PEOPLE values (7, 'Giovannia', 'Ney', 'totaiuoly@legit.email', 2, 'Trappa', 'Toni',true);
insert into PEOPLE values (11, 'Luigi', 'Mario', 'tllsy@legit.email', 2, 'Luigi', 'Peach',true);
insert into PEOPLE values (12, 'Mario', 'Mario', 'toly@legst.email', 2, 'Mario', 'Peach',true);
insert into PEOPLE values (13, 'Toni', 'Doni', 'tioly@legit.email', 2, 'Toni', 'NumberTen',true);
		--San Franco
insert into PEOPLE values (14, 'San', 'Marco', 'onely@legit.email', 2, 'Marco', 'Darco',true);
insert into PEOPLE values (15, 'Fernand', 'Doh', 'ason@legit.email', 2, 'Fer', 'Lew',true);
insert into PEOPLE values (16, 'Count', 'Mondego', 'the@legst.email', 2, 'King', 'Pawn',true);
insert into PEOPLE values (17, 'His', 'Father', 'special@legit.email', 2, 'Yaeh', 'Okay',true);
		--Moskva
insert into PEOPLE values (18, 'Yulia', 'Bordarenko', 'yeallow@blue.uk', 2, 'Yulia', 'Tsezar',true);
insert into PEOPLE values (19, 'Dzhulietta', 'Stoichkova', 'hit@the.net', 2, 'Spell', 'This',true);
insert into PEOPLE values (20, 'Romeo', 'Ivankovich', 'the@chosen.one', 2, 'Random', 'Word',true);
insert into PEOPLE values (21, 'Nick', 'Slaughter', 'tropical@heat.zarkovo', 2, 'Tropical', 'Sindy',true);
insert into PEOPLE values (22, 'Wisla', 'Pedenko', 'sole@pole.pl', 2, 'Wisla', 'Krakow',true);
insert into PEOPLE values (23, 'Ivan', 'Grozny', 'notthecommie@red.ru', 2, 'Ivan', 'Hammer',true);
	--Hyatt
insert into PEOPLE values (24, 'Fancy', 'Pants', 'sos@psle.pl', 2, 'Fancy', 'Mancy',true);
insert into PEOPLE values (25, 'Pret', 'Ending', 'me@account.com', 2, 'Pret', 'Entious',true);
	--Blue Lagoon
insert into PEOPLE values (26, 'Patrick', 'Star', 'I@actually.live', 2, 'Under', 'aRock',true);
insert into PEOPLE values (27, 'Sqidward', 'Tentacles', 'You@go.away', 2, 'Barnecle', 'Brain',true);
insert into PEOPLE values (28, 'Sponge Bob', 'Square Pants', 'I@am.ready', 2, 'Pine', 'Apple',true);
insert into PEOPLE values (29, 'Sandy', 'Last Name', 'Winter@coming.is', 2, 'Karate', 'Kid',true);
insert into PEOPLE values (30, 'Kabbislav K', 'Krabbstein', 'money@money.money', 2, 'Money', 'Money',true);
--owners 
insert into PEOPLE values (3, 'Lucio', 'Alternativo', 'toually@legit.email',1, 'guilermo', 'secret',true);
insert into PEOPLE values (4, 'Francesco', 'Franco', 'toggly@legit.email', 1, 'ralph', 'strong', true);
insert into PEOPLE values (8, 'Vlad', 'Jakov', 'stvarni@email.ru', 1, 'vlad', 'tayna', true);
insert into PEOPLE values (9, 'Borko', 'Stanis', 'ja@email.so', 1, 'bor', 'jina', true);
insert into PEOPLE values (10, 'Janko', 'Crown', 'cramota@tot.co.uk', 1, 'vishy', 'maggie', true);
						--id, owner, name
insert into HOTEL_CHAINS values (1, 3, 'Monte Carlo');
insert into HOTEL_CHAINS values (2, 4, 'San Franco');
insert into HOTEL_CHAINS values (3, 8, 'Moskva');
insert into HOTEL_CHAINS values (4, 9, 'Hyatt');
insert into HOTEL_CHAINS values (5, 10,'Blue Lagoon');
						--id, chain, name, manager
	--Monte Carlo
insert into HOTELS values (1, 1, 'Lucca', 6, 'Sydney');
insert into HOTELS values (2, 1, 'Dubadubai', 5, 'Melbourne');
insert into HOTELS values (3, 1, 'Truilli', 7, 'Brisbane');
insert into HOTELS values (4, 1, 'Mira', 11, 'Perth');
insert into HOTELS values (5, 1, 'Casino Roma', 12, 'Hobart');
insert into HOTELS values (6, 1, 'Tropico', 13, 'Adelaide');
	-- San Franco
insert into HOTELS values (7, 2, 'Las Palmas', 14, 'Sydney');
insert into HOTELS values (8, 2, 'Los Tomatos', 15, 'Melbourne');
insert into HOTELS values (9, 2, 'La Grande Vittoria', 16, 'Brisbane');
insert into HOTELS values (10, 2,'La Verde Tinha', 17, 'Perth');
	--Moskva
insert into HOTELS values (11, 3, 'Krasniy Oktobar', 18, 'Sydney');
insert into HOTELS values (12, 3, 'Petar Velikiy', 19, 'Melbourne');
insert into HOTELS values (13, 3, 'Pobyeda', 20, 'Brisbane');
insert into HOTELS values (14, 3, 'Mostowsky', 21, 'Perth');
insert into HOTELS values (15, 3, 'Segodnya', 22, 'Hobart');
insert into HOTELS values (16, 3, 'Sto lyet', 23, 'Adelaide');
	--Hyatt
insert into HOTELS values (18, 4, 'Sydney Hyatt', 24, 'Sydney');
insert into HOTELS values (19, 4, 'Peth Hyatt', 25, 'Perth');
	--Blue Lagoon
insert into HOTELS values (20, 5, 'Harbour Lagoon', 26, 'Sydney');
insert into HOTELS values (22, 5, 'Albert Park Lagoon', 27, 'Melbourne');
insert into HOTELS values (23, 5, 'Hillarys Lagoon', 28, 'Perth');
insert into HOTELS values (24, 5, 'Snowy Hills Lagoon', 29, 'Hobart');
insert into HOTELS values (21, 5, 'Port Adelaide Lagoon', 30, 'Adelaide');
					--id, hotel, num, type ,main
insert into ROOMS values (1, 1, 1, 1, false);
insert into ROOMS values (2, 1, 2, 1, false);
insert into ROOMS values (3, 1, 3, 1, false);
insert into ROOMS values (4, 1, 4, 3, false);
insert into ROOMS values (5, 2, 1, 1, false);
insert into ROOMS values (6, 2, 2, 2, false);
insert into ROOMS values (7, 2, 3, 4, false);
insert into ROOMS values (8, 3, 1, 5, false);
insert into ROOMS values (9, 3, 2, 5, false);
insert into ROOMS values (10, 3, 3, 5,false);
insert into ROOMS values (11, 4, 1, 1, false);
insert into ROOMS values (12, 4, 2, 1, false);
insert into ROOMS values (13, 4, 3, 1, false);
insert into ROOMS values (14, 4, 4, 3, false);
insert into ROOMS values (15, 5, 1, 1, false);
insert into ROOMS values (16, 5, 2, 2, false);
insert into ROOMS values (17, 5, 3, 4, false);
insert into ROOMS values (18, 6, 1, 5, false);
insert into ROOMS values (19, 6, 2, 5, false);
insert into ROOMS values (20, 6, 3, 5,false);
--San Franco
insert into ROOMS values (21, 7, 1, 1, false);
insert into ROOMS values (22, 7, 2, 1, false);
insert into ROOMS values (23, 7, 3, 2, false);
insert into ROOMS values (24, 8, 1, 2, false);
insert into ROOMS values (25, 8, 2, 1, false);
insert into ROOMS values (26, 8, 3, 1, false);
insert into ROOMS values (27, 9, 1, 2, false);
insert into ROOMS values (28, 9, 2, 1, false);
insert into ROOMS values (29, 10, 1, 1, false);
insert into ROOMS values (30, 10, 2, 2,false);
--Moskva
insert into ROOMS values (31, 11, 1, 4, false);
insert into ROOMS values (32, 11, 2, 4, false);
insert into ROOMS values (33, 11, 3, 5, false);
insert into ROOMS values (34, 11, 4, 5, false);
insert into ROOMS values (35, 11, 5, 4, false);
insert into ROOMS values (36, 12, 1, 5, false);
insert into ROOMS values (37, 12, 2, 4, false);
insert into ROOMS values (38, 12, 3, 1, false);
insert into ROOMS values (39, 12, 4, 4, false);
insert into ROOMS values (40, 12, 5, 5,false);
insert into ROOMS values (41, 13, 1, 1, false);
insert into ROOMS values (42, 13, 2, 1, false);
insert into ROOMS values (43, 13, 3, 1, false);
insert into ROOMS values (44, 14, 1, 3, false);
insert into ROOMS values (45, 14, 2, 1, false);
insert into ROOMS values (46, 14, 3, 2, false);
insert into ROOMS values (47, 14, 4, 4, false);
insert into ROOMS values (48, 15, 1, 5, false);
insert into ROOMS values (49, 15, 2, 5, false);
insert into ROOMS values (50, 15, 3, 5,false);
insert into ROOMS values (51, 16, 1, 1, false);
insert into ROOMS values (52, 16, 2, 1, false);
insert into ROOMS values (53, 16, 3, 1, false);
insert into ROOMS values (54, 16, 4, 1, false);
	--Hyatt
insert into ROOMS values (55, 18, 1, 1, false);
insert into ROOMS values (56, 18, 2, 1, false);
insert into ROOMS values (57, 18, 3, 4, false);
insert into ROOMS values (58, 18, 4, 5, false);
insert into ROOMS values (59, 19, 1, 1, false);
insert into ROOMS values (60, 19, 2, 1,false);
insert into ROOMS values (61, 19, 3, 2, false);
insert into ROOMS values (62, 19, 4, 3, false);
insert into ROOMS values (63, 19, 5, 5, false);
	--Blue Lagoon
insert into ROOMS values (64, 20, 1, 3, false);
insert into ROOMS values (65, 20, 2, 1, false);
insert into ROOMS values (66, 20, 3, 2, false);
insert into ROOMS values (67, 20, 4, 4, false);
insert into ROOMS values (68, 21, 1, 5, false);
insert into ROOMS values (69, 21, 2, 5, false);
insert into ROOMS values (70, 21, 3, 5,false);
insert into ROOMS values (71, 21, 4, 1, false);
insert into ROOMS values (72, 21, 5, 3, false);
insert into ROOMS values (73, 22, 1, 3, false);
insert into ROOMS values (74, 22, 2, 3, false);
insert into ROOMS values (75, 22, 3, 1, false);
insert into ROOMS values (76, 23, 1, 2, false);
insert into ROOMS values (77, 23, 2, 4, false);
insert into ROOMS values (78, 23, 3, 5, false);
insert into ROOMS values (79, 23, 4, 5, false);
insert into ROOMS values (80, 23, 5, 5,false);
insert into ROOMS values (81, 24, 1, 1, false);
insert into ROOMS values (82, 24, 2, 3, false);
insert into ROOMS values (83, 24, 3, 4, false);
insert into ROOMS values (84, 24, 4, 3, false);


																--hotel, type, value, start, enddate (YYYY-MM-DD)
insert into DISCOUNTS(hotel, roomtype, val, startdate, enddate) values (1, 1, 20, '2006-11-15', '2006-11-17');
insert into DISCOUNTS(hotel, roomtype, val, startdate, enddate) values (1, 1, 20, '2006-12-14', '2006-12-24');
																--hotel, surcharge, startdate, enddate
insert into PEAKS(hotel, surcharge, startdate, enddate) values (1, 20, '2006-12-01', '2006-12-31');
insert into PEAKS(hotel, surcharge, startdate, enddate) values (2, 20, '2006-12-07', '2006-12-24');
insert into PEAKS(hotel, surcharge, startdate, enddate) values (3, 20, '2006-11-26', '2007-01-31');
insert into PEAKS(hotel, surcharge, startdate, enddate) values (1, 20, '2007-03-08', '2007-03-22');
					--bookee, room, extrabed, start_date, end_date, total_cost, checkedIN
insert into BOOKINGS(bookee, room, extrabed, start_date, end_date, total_cost, checkedIN)
					values(6, 4, false, '2006-12-14', '2006-12-16', 160, false);
insert into BOOKINGS(bookee, room, extrabed, start_date, end_date, total_cost, checkedIN)
					values(7, 5, false, '2006-12-14', '2006-12-16', 160, false);
insert into BOOKINGS(bookee, room, extrabed, start_date, end_date, total_cost, checkedIN)
					values(3, 2, false, '2006-12-15', '2006-12-19', 160, false);
insert into BOOKINGS(bookee, room, extrabed, start_date, end_date, total_cost, checkedIN)
					values(3, 4, false, '2006-12-12', '2006-12-14', 160, false);
insert into BOOKINGS(bookee, room, extrabed, start_date, end_date, total_cost, checkedIN)
					values(4, 4, false, '2006-12-21', '2006-12-28', 160, false);
					
