							--id, type, price, extabed?
insert into ROOM_TYPES VALUES (1, 'single', 80, false);
insert into ROOM_TYPES VALUES (2, 'twin', 130, true);
insert into ROOM_TYPES VALUES (3, 'queen', 150, true);
insert into ROOM_TYPES VALUES (4, 'executive', 200, true);
insert into ROOM_TYPES VALUES (5, 'suite', 320, true);

insert into ROLES VALUES (1, 'owner');
insert into ROLES VALUES (2, 'manager');
insert into ROLES VALUES (3, 'customer');