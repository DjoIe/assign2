SELECT h.id, h.name, p.firstname, p.username, p.password, p.profile FROM HOTEL_CHAINS h JOIN people p ON (owner = p.id);

SELECT * FROM people;
Select * FROM profiles;

create view roomOwners(id, number, roomname, hotelname, chainname, ownerid, owner) as 
SELECT G.id, g.num,  g.roomname, g.hotelname, g.chainname, p.id as ownerid, p.firstname as ownername FROM (
SELECT H.id, H.num, h.hotelname, h.name as roomname, c.owner, c.name as chainname  FROM (
SELECT T.id, T.num, T.chain, T.hotelname, r.name FROM 
(SELECT r.id, r.num, r.type, h.chain, h.name as hotelname FROM rooms r JOIN hotels h ON (hotel = h.id)) AS T
JOIN room_types r ON (type = r.id)) AS H
JOIN HOTEL_CHAINS c ON (chain = c.id))AS G
JOIN PEOPLE p ON (OWNER = p.id)
;

SELECT * FROM roomOWNERS;
Select * FROM PersonData;
select * FROM Discounts;
select * from peaks;

-- prices view, get current price function , in progress
select q.id, q.hotel, q.num, q.type, q.name, q.baseprice, q.extrabed, q.val, q.disStartDate, q.disEndDate, p.surcharge, p.startdate as peakStartDate, p.enddate as peakEndDate from
(select h.id, h.hotel, h.num, h.type, h.name, h.baseprice, h.extrabed, d.val, d.startdate as disStartDate, d.enddate as disEndDate from 
(select r.id,  r.hotel, r.num, r.type, r.inMaintenence, t.name, t.basePrice, t.extrabed 
FROM rooms r JOIN room_types t ON (r.type = t.id)) as H
LEFT JOIN discounts d ON (h.type = d.roomtype AND h.hotel = d.hotel)
where inmaintenence = false) AS Q
LEFT JOIN peaks p ON (q.hotel = p.hotel);

DROP VIEW roomOwners;