create view PersonData(id, firstName, lastName, nickname, email, username, password, role, creditCard, address, phone) as(
 Select p.id, p.firstName, p.lastname, r.nickname, p.email, p.username, p.password, p.role, r.creditCardNo, r.address, r.phoneNumber 
 FROM people p LEFT JOIN profiles r on (p.id = r.person)
 );
 

--used for prices
create view roomsDataRaw as (
select t.id, t.hotel, t.num, t.type, t.name, t.baseprice, t.extrabed, t.val, t.disStartDate, t.disEndDate, t.surcharge, t.peakstartdate, t.peakenddate, h.name as hotelname, h.city from
(select q.id, q.hotel, q.num, q.type, q.name, q.baseprice, q.extrabed, q.val, q.disStartDate, q.disEndDate, p.surcharge, p.startdate as peakStartDate, p.enddate as peakEndDate from
(select h.id, h.hotel, h.num, h.type, h.name, h.baseprice, h.extrabed, d.val, d.startdate as disStartDate, d.enddate as disEndDate from 
(select r.id,  r.hotel, r.num, r.type, r.inMaintenence, t.name, t.basePrice, t.extrabed 
FROM rooms r JOIN room_types t ON (r.type = t.id)) as H
LEFT JOIN discounts d ON (h.type = d.roomtype AND h.hotel = d.hotel)
where inmaintenence = false) AS Q
LEFT JOIN peaks p ON (q.hotel = p.hotel)) AS T
LEFT JOIN HOTELS h on (t.hotel = h.id)
);

--used for bookings
--usage : select * from roomBookings 
--		  where ((start_date <= [BOOKING_START_DATE] and end_date > [BOOKING_END_DATE]) or
--		(start_date <= [BOOKING_END_DATE] and start_date >=[BOOKING_START_DATE] )) or start_date is NULL
--		and hotel = [BOOKING_HOTEL_ID] and inMaintenence = false and bookings.id is null;
--
-- //it will return a table of all rooms in the desired hotel
	
--Here's and example, [HOTEL_ID] = 1, [BOOKING_START_DATE] = 2006-12-14, [BOOKING_END_DATE] = 2006-12-17
--select * from roomBookings
--	where (((start_date <= '2006-12-14' and end_date > '2006-12-14') or
--	(start_date <= '2006-12-17' and start_date >='2006-12-14' )) or start_date is NULL)
--	and hotel = 1 and inMaintenence = false and bid is null;
--*/

create view roomBookings as(
	select r.id, r.hotel, r.num, r.type, r.inmaintenence, b.id as bid, b.bookee, b.room, b.extrabed, b.start_date, b.end_date, b.total_cost, b.checkedin 
	from rooms r LEFT JOIN bookings b ON (r.id = room)
);

--/* USE "select count(1) from ( EXPR )" to get the number of available rooms
-- here's an example, with the EXPR from above
--select count(1) from
--	(select * from roomBookings
--	where (((start_date <= '2006-12-14' and end_date > '2006-12-14') or
--	(start_date <= '2006-12-17' and start_date >='2006-12-14' )) or start_date is NULL)
--	and hotel = 1 and inMaintenence = false and bid is null);

--which returns 2,
--*/

	
create view managerData(id, firstname, lastname, email, username, password, creditcardno, nickname, address, phonenumber) as (
SELECT g.id, g.firstname, g.lastname, g.email, g.username, g.password, p.creditcardno, p.nickname, p.address, p.phonenumber FROM
(select * from people
where role = '2') as G LEFT JOIN profiles p ON (G.id = p.person)
);

create view ownerData(id, firstname, lastname, email, username, password, creditcardno, nickname, address, phonenumber) as (
SELECT g.id, g.firstname, g.lastname, g.email, g.username, g.password, p.creditcardno, p.nickname, p.address, p.phonenumber FROM
(select * from people
where role = '1') as G LEFT JOIN profiles p ON (G.id = p.person)
);

