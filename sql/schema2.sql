create table roles (
	id 				integer, -- PG: serial
	name			varchar(20),
	primary key (id)
);
 
create table profiles (
	id 			integer, -- PG: serial;
	creditCardNo varchar(16),
	nickname	varchar (30),
	address		varchar(200),
	phoneNumber varchar(20),
	primary key (id)
);

create table room_types (
	id				integer, -- PG: serial
	name 			varchar(20),
	capacity			integer,
	base_cost		integer,
	primary key (id)
);

create table people (
	id 				integer, -- PG: seria;
	firstName		varchar(40) not null,
	lastName		varchar(40) not null,
	email			varchar(100) not null,
	role 			integer not null,
	username		varchar(50) not null,
	password		varchar(32) not null,
	profile 		integer,
	--foreign key (role) references roles(id),
	--foreign key (profile) references profiles(id),
	primary key (id)
);

create table hotel_chains (
	id				integer, -- PG: serial
	owner			integer not null,
	name			varchar(50) not null,
	foreign key (owner) references people(id),
	primary key (id)
);

create table hotels (
	id          integer, -- PG: serial
	chain       integer not null,
	name        varchar(50) not null,
	manager		integer not null,
	foreign key (chain) references hotel_chains(id),
	foreign key (manager) references people(id),
	primary key (id)
);

create table peaks (
	id          integer, -- PG: serial
	hotel       integer not null,
	surcharge   integer not null,
	startDate	date not null,
	endDate		date not null,
	foreign key (hotel) references hotels(id),
	primary key (id)
);


create table rooms(
	id				integer, -- PG: serial
	hotel			integer not null,
	num				integer not null, --room name
	type			integer not null,
	foreign key (hotel) references hotels(id),
	foreign key (type) references room_types(id),
	primary key (id)
);

create table discounts (
	id				integer, -- PG: serial
	hotel			integer not null,
	roomType		integer not null,
	val	 		integer check (val > 0 and val < 100),
	foreign key (hotel) references hotels(id),
	foreign key (roomType) references room_types(id),
	primary key (id)
);

create table bookings (
	id 			integer, -- PG: serial;
	Bookee		integer not null,
	room		integer not null,
	extraBed    boolean default false,
	start_date	date not null,
	end_date	date not null,
	total_cost	integer,
	foreign key (Bookee)  references people(id),
	foreign key (room) references rooms(id),
	primary key (id)
);
