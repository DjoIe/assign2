drop view ownerData;
drop view managerData;
drop view roomsDataRaw;
drop view Persondata;
drop view roomBookings;

drop table activationWL;
drop table bookings;
drop table discounts;
drop table rooms;
drop table peaks;
drop table hotels;
drop table hotel_chains;
drop table people;
drop table room_types;
drop table profiles;
drop table roles;