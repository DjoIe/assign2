<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Manage Maintenance</title>

<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script>
$(function() {
    $("#datepicker_in").datepicker();
    $("#datepicker_in").datepicker("option", "dateFormat", "dd/MM/yy");
    $("#datepicker_out").datepicker();
    $("#datepicker_out").datepicker("option", "dateFormat", "dd/MM/yy");
    
    $("#datepicker_in").val("${startdate}");
    $("#datepicker_out").val("${enddate}");
});
</script>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="owner_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="owner_header_loggedin.html" %>
	<%
}
%>
<h1>HOTEL: <c:out value="${hotel.name}" /></h1>
<h2>Rooms in Maintenance</h2>

<form method = "post" action = "RemoveMaintenance">
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<tr><td>Room Number</td><td>Room Type</td><td>Remove Maintenance</td></tr>
<c:forEach var="r" items="${hotel.inMaintenance}">
 		<tr>
 			<td><c:out value="${r.roomNumAvailable}" /></td>
     		<td><c:out value="${r.roomType}" /></td>
     		<td><input type = "checkbox" name = "removeMaintenance" value = "${r.id}"/></td>
     	</tr>
</c:forEach>
<c:if test="${not empty hotel.inMaintenance}">
	<tr><td><input type = "submit" name = "mbutton" value = "Remove Maintenance on Selected Rooms">
	<input type = "hidden" name = "hid" value = "${hotel.id}"></td></tr>
</c:if>
<c:if test="${empty hotel.inMaintenance}">
	<tr><td>No rooms currently in maintenance</td></tr>
</c:if>
</table>
</form>

<h2>Vacant Rooms</h2>
<form method = "post" action = "SetMaintenance">
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<tr><td>Room Number</td><td>Room Type</td><td>Set Maintenance</td></tr>
<c:forEach var="r" items="${hotel.vacantRooms}">
 		<tr>
 			<td><c:out value="${r.roomNumAvailable}" /></td>
     		<td><c:out value="${r.roomType}" /></td>
     		<td><input type = "checkbox" name = "setMaintenance" value = "<c:out value="${r.id}" />"></td>
     	</tr>
</c:forEach>
<c:if test="${not empty hotel.vacantRooms}">
	<tr><td><input type = "submit" name = "mbutton" value = "Set Maintenance on Selected Rooms">
	<input type = "hidden" name = "hid" value = "${hotel.id}"></td></tr>
</c:if>
<c:if test="${empty hotel.vacantRooms}">
	<tr><td>No vacant rooms</td></tr>
</c:if>
</table>
</form>

</body>
</html>