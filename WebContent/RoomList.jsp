<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Assign Room</title>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="manager_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="manager_header_loggedin.html" %>
	<%
}
%>
<h2>CUSTOMER</h2>
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<tr><td>Last Name</td><td>First Name</td><td>Room Type</td><td>Check in Date</td><td>Check out Date</td></tr>
<tr>
     		<td><c:out value="${guest.lastName}" /></td>
     		<td><c:out value="${guest.firstName}" /></td>
     		<td><c:out value="${roomType}" /></td>
     		<td><c:out value="${guest.checkInDate}" /></td>
     		<td><c:out value="${guest.checkOutDate}" /></td>
     	</tr>
     	</table>
<h2>VACANT ROOMS</h2>

<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<c:if test="${not empty hotel.vacantRooms}">
<tr><td>Room Number</td><td>Room Type</td><td>Assign to Room</td></tr>
<c:forEach var="aRoom" items="${hotel.vacantRooms}">
 		
			<c:if test="${roomType eq aRoom.roomType}">
			<tr>
 			<td><c:out value="${aRoom.roomNumAvailable}" /></td>
 			<td><c:out value="${aRoom.roomType}" /></td>
     		<td>
     		<form method = "post" name = "assignForm" action = "AssignRoom">
     		<input type = "submit" name = assignRooms value = "Assign To Room">
     		<input type = "hidden" name = roomToAssign value = "${aRoom.id}">
     		<input type = "hidden" name = bid value = "${bid}">
     		</form>
     		</td>
     		</tr>
     		</c:if>
     	
	</c:forEach>
</c:if>
<c:if test="${empty hotel.vacantRooms}">
<tr><td>No Vacant Rooms</td></tr>
</c:if>
</table>


</body>
</html>