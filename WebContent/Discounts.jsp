<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Hotel Management</title>

<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script>
$(function() {
    $("#datepicker_in").datepicker();
    $("#datepicker_in").datepicker("option", "dateFormat", "dd/MM/yy");
    $("#datepicker_out").datepicker();
    $("#datepicker_out").datepicker("option", "dateFormat", "dd/MM/yy");
    
    $("#datepicker_in").val("${startdate}");
    $("#datepicker_out").val("${enddate}");
});
</script>
</head>
<body>
<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="owner_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="owner_header_loggedin.html" %>
	<%
}
%>
<table><tr><td><font color="#ff0000"><c:out value="${msg}" /></font></td></tr></table>
<h1>HOTEL: <c:out value="${hotel.name}" /></h1>
<h2>Discounts</h2>
<form method = "post" action = "RemoveDiscount">
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<c:if test="${not empty discounts}">
<tr><td>Hotel</td><td>Room Type</td><td>Discount</td><td>Start Date</td><td>End Date</td><td>Remove Discount</td></tr>
<c:forEach var="d" items="${discounts}">
 		<tr>
 			<td><c:out value="${d.hotel}" /></td>
     		<td><c:out value="${d.roomType}" /></td>
     		<td>%<c:out value="${d.value}" /> off</td>
     		<td><c:out value="${d.startDate}" /></td>
     		<td><c:out value="${d.endDate}" /></td>
     		<td><input type = "checkbox" name = "removeDiscount" value = "${d.id}"></td>
     	</tr>
</c:forEach>
	<tr><td><input type = "submit" name = "dbutton" value = "Remove Selected Discounts">
	<input type = "hidden" name = "hid" value = "${hotel.id}"></td></tr>
</c:if>
<c:if test="${empty discounts}">
	<tr><td>No Discounts on Hotel</td></tr>
</c:if>
</table>
</form>
<h2>Add New Discount</h2>
<form method = "post" action = "AddDiscount">
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
     	<tr><td>ROOM TYPE: </td>
 			<td><select name="roomType">
 				<option value = "1">SINGLE</option>
 				<option value = "2">TWIN</option>
 				<option value = "3">QUEEN</option>
 				<option value = "4">EXECUTIVE</option>
 				<option value = "5">SUITE</option>
 			</select></td>
     	</tr>
     	<tr><td>DISCOUNT: </td>
 			<td><select name="discount">
 				<option value = "5">5%</option>
 				<option value = "10">10%</option>
 				<option value = "20">20%</option>
 				<option value = "50">50%</option>
 				<option value = "75">75%</option>
 			</select></td>
 		<tr><td>START/END DATE: </td>
 			<td><input type="text" id="datepicker_in" name="discountStart" value="${startdate}"></td>
 			<td><input type="text" id="datepicker_out" name="discountEnd" value="${enddate}"></td>
     	</tr>
     	<tr><td><input type = "submit" value = "Add Discount">
     			<input type = "hidden" name = "hid" value = "${hotel.id}"></td></tr>
</table>
</form>
</body>
</html>