<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.enterprise.beans.*, java.util.*"%>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<jsp:useBean id="bookingSearchRes" class="com.enterprise.beans.BookingSearchBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">

<title>Bookings.Yeah! Confirm Booking Change</title>
</head>
<body>
<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>
<form action="UpdateBooking" method="post">
<% ArrayList<BookingsBeans> changedBookings = bookingSearchRes.getUpdatedBookings();
System.out.println("Num updated bookings: " + changedBookings.size());
	String displayStr = "";
	String bedStr = "";
	if (!changedBookings.isEmpty()) {
		BookingsBeans currBooking = changedBookings.get(0);
		System.out.println("booking deets: " + currBooking.getBookee() + " " + currBooking.getStartDate().toString() + " " + currBooking.getEndDate().toString());
		
		if (currBooking.getCancelStatus()) {
			// we're cancelling
			displayStr = "The following bookings will be cancelled.";
		}
		else {
			displayStr = "The following modifications will be made to the bookings.";
		}
		%>
		<p><%=displayStr%></p>
		<%
		for (int i = 0; i < changedBookings.size(); i++) {
			currBooking = changedBookings.get(i);
			if (currBooking.getExtraBed()) {
				bedStr = "Includes extra bed";
			}
			else {
				bedStr = "Does not include extra bed";
			}
		%>
		<div class="w3-container w3-card-4">
	<div class="w3-group">
		<label class="w3-label"><%=currBooking.getHotelName() %></label>
	</div>
	<div class="w3-group">
			<label class="w3-label">Updated Start Date:</label>
		<label class="w3-label"><%=currBooking.getStartDateString() %></label>
	</div>
	<div class="w3-group">
		<label class="w3-label">Updated End Date:</label>
		<label class="w3-label"><%=currBooking.getEndDateString() %></label>
	</div>
	<div class="w3-group">
			<label class="w3-label">Include bed?</label>
		<label class="w3-label"><%=bedStr %></label>
	</div>
	<div class="w3-group">
			<label class="w3-label">Updated Cost: </label>
		<label class="w3-label"><%=currBooking.getCost() %></label>
	</div>
	</div>
		<%
		} %>
		<input type="submit" name="act" value="Confirm booking change">
		<input type="submit" name="act" value="Cancel all changes">
	
	<%
	}
	else {
		%>
		<p> No booking needs modification! </p>
		<%
	}
%>
</form>
</body>
</html>