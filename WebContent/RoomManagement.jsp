<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page import="java.util.*"%>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Room Management</title>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="manager_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="manager_header_loggedin.html" %>
	<%
}
%>

<h3>Occupied Rooms</h3>
<form method = "post" name = "unassignForm" action = "UnassignRoom">
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<c:if test="${not empty hotel.assignedBookings}">
<tr><td>Room Number</td><td>Room Type</td><td>Last Name</td>
<td>First Name</td><td>Check in Date</td><td>Check out Date</td><td>Unassign Room</td></tr>
<c:forEach var="booking" items="${hotel.assignedBookings}">
 		<tr>
 			<td><c:out value="${booking.roomNum}" /></td>
     		<td><c:out value="${booking.roomType}" /></td>
     		<td><c:out value="${booking.lastName}" /></td>
     		<td><c:out value="${booking.firstName}" /></td>
     		<td><c:out value="${booking.checkInDate}" /></td>
     		<td><c:out value="${booking.checkOutDate}" /></td>
     		<td><input  type="checkBox" name="bookingToUnassign" value="${booking.bid}" ></td>
     	</tr>
	</c:forEach>
	<tr><td><input type = "submit" name = unassignRooms value = "Unassign Selected Rooms"></td></tr>
</c:if>
<c:if test="${empty hotel.assignedBookings}">
<tr><td>No Occupied Rooms</td></tr>
</c:if>
</table>
</form>
<h3>Unassigned Bookings</h3>

<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<c:if test="${not empty hotel.unassignedBookings}">
<tr><td>Last Name</td><td>First Name</td><td>Room Type<td>Check in Date</td><td>Check out Date</td></tr>
<c:forEach var="booking" items="${hotel.unassignedBookings}">
 		<tr>
     		<td><c:out value="${booking.lastName}" /></td>
     		<td><c:out value="${booking.firstName}" /></td>
     		<td><c:out value="${booking.roomType}" /></td>
     		<td><c:out value="${booking.checkInDate}" /></td>
     		<td><c:out value="${booking.checkOutDate}" /></td>
     		<td>
     		<form method = "post" name = "assignForm" action = "RoomList">
     		<input type = "hidden" name = "roomType" value = "${booking.roomType }">
     		<input type = "hidden" name = "bookingToAssign" value = "${booking.bid }">
     		<input type = "submit" name = "assignRooms" value = "Assign To Room">
     		</form>
     		</td>
     	</tr>
	</c:forEach>
</c:if>
<c:if test="${empty hotel.unassignedBookings}">
<tr><td>No unassigned bookings</td></tr>
</c:if>
</table>


</body>
</html>