<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.enterprise.beans.*, java.util.*"%>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="bookingSearchRes" class="com.enterprise.beans.BookingSearchBean" scope="session" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<title>Bookings.Yeah! Edit Bookings</title>
<script>
$(function() {
    $("#datepicker_in").datepicker();
    $("#datepicker_in").datepicker("option", "dateFormat", "dd/MM/yy");
    $("#datepicker_out").datepicker();
    $("#datepicker_out").datepicker("option", "dateFormat", "dd/MM/yy");
    
    $("#datepicker_in").val("${startdate}");
    $("#datepicker_out").val("${enddate}");
});

</script>
</head>
<body>
<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>

<form action='ModifyBookings' method="post">
<% 
BookingsBeans currBooking = null;
for(int i = 0; i < bookingSearchRes.getBookingsToEdit().size(); i++) {
	currBooking = bookingSearchRes.getBookingsToEdit().get(i);
%>
<div class="w3-container w3-card-4">
	<div class="w3-group">
		<label class="w3-label"><%currBooking.getHotelName(); %></label>
	</div>
	<div class="w3-group">
		<input type="text" id="datepicker_in" name="startDate" value="${startdate}">
    	<label class="w3-label w3-validate">Start Date (dd/mm/yy)</label>
	</div>
	<div class="w3-group">
		<input type="text" id="datepicker_out" name="endDate" value="${enddate}">
    	<label class="w3-label w3-validate">End Date (dd/mm/yy)</label>
	</div>
	<div class="w3-group">
		<label class="w3-label">Extra Bed?</label>
		<input id="Yes" class="w3-radio" type="radio" name="extraBed" value="Yes" >
    	<label class="w3-validate">Yes</label>
        
        <input id="No" class="w3-radio" type="radio" name="extraBed" value="No" checked>
    	<label class="w3-validate">No</label>
	</div>
</div>

<%
}
%>

<input type="submit" name="act" value="Update Bookings">
<input type="submit" name="act" value="Cancel all changes">
</form>


</body>
</html>