<%@page import="com.enterprise.beans.BookingBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.enterprise.beans.*, java.util.*, java.text.SimpleDateFormat, 
    java.text.DateFormat;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="checkout" class="com.enterprise.beans.CartBean" scope="session" />
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bookings.Yeah! Cart</title>
<link rel="stylesheet" href="css/search.css">
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
    if(user.getUserName() == null) {
    // include user_header.html
    %>
    <%@ include file="user_header.html" %>
    <%
}
else {
    // include user_header_loggedin.html
    %>
    <%@ include file="user_header_loggedin.html" %>
    <%
}
%>
<body>
    <div class="center">
        <form action='CartCheckoutPay' method="post">
            <h3>Order Detail:</h3>
            <table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
                <tr>
                    <th>Hotel</th>
                    <th>Room Type</th>
                    <th>Room Num</th>
                    <th>Price</th>
                    <th>Period</th>                
                    <th>Extra bed</th>
                    <th>Checkin Date</th>
                    <th>Checkout Date</th>
                </tr>
        
                <%
                    DateFormat df = new SimpleDateFormat("dd/MMMM/yy");
                    BookingBean booking = null;
                    for (int i = 0; i < checkout.getBookings().size(); i++) {
                        
                        booking = (BookingBean) checkout.getBookings().get(i);
                        
                        String startDate = df.format(booking.getStartDate());
                        String endDate = df.format(booking.getEndDate());
                %>
                    <tr>
                        <td><%=booking.getRoom().getHotel()%></td>
                        <td><%=booking.getRoom().getRoomType()%></td>
                        <td><%=booking.getRoomnum()%></td>
                        <td><%=booking.getRoom().getPrice()%></td>
                        <td><%=booking.getRoom().getPeriod()%></td>
                        
                        <td><%=booking.GetExtrabed()%></td>
                        <td><%=startDate%></td>
                        <td><%=endDate%></td>
                    </tr>
                
                <%
                    }
                %>
            </table>
            
            <div style="padding:10px 0;">
                <a>Total Price: <%=checkout.getTotalCost()%></a>
            </div>
            <br>
            
            <h3>Payment Information:</h3>
            <table>
                <tr>
                    <td>*Credit card:</td>
                    <td><input type="text" id="datepicker_in" name="creditcard" value="<%=user.getCreditCard() %>"></td>
                </tr>
                <tr>
                    <td>*First Name:</td>
                    <td><input type="text" id="datepicker_out" name="firstname" value="<%=user.getFirstName() %>"></td>
                </tr>
                <tr>
                    <td>*Last Name:</td>
                    <td><input type="text" id="datepicker_out" name="lastname" value="<%=user.getLastName() %>"></td>
                </tr>
                <tr>
                    <td>*Address:</td>
                    <td><input type="text" id="city" name="address" value="<%=user.getAddress() %>"></td>
                </tr>
                <tr>
                    <td>*Email:</td>
                    <td><input type="text" name="email" value="<%=user.getEmail() %>"></td>
                </tr>
            </table>
            
            <br>
            <input type="submit" value="Pay">
            <br>
            
            <%
	           String alert = (String) request.getAttribute("alert");
	           if(alert == null){
	               alert = "";
	           }
	         %>
	        <a class="alert"><%=alert %></a>
        </form>
    </div>
</body>
</html>