<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean"
	scope="session" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bookings.Yeah! Login/Signup</title>
</head>
<body>
<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>

<table><tr><td><font color="#ff0000"><c:out value="${error}" /></font></td></tr></table>
<form method="post" name="Login" action="Login">
	<h1>LOGIN</h1>
	<table>
	<tr><td>Username</td></tr>
	<tr><td><input type="text" name = "Username"></td></tr>
	<tr><td>Password</td></tr>
	<tr><td><input type="password" name = "Password"></td></tr>
	<tr><td><input type="submit" value = "Login"></td></tr>
	</table>
</form>
<form method="post" name="Signup" action="SignUp">
	<h1>SIGN UP</h1>
	<table>
	<tr><td>First Name</td></tr>
	<tr><td><input type="text" name = "Firstname"></td></tr>
	<tr><td>Last Name</td></tr>
	<tr><td><input type="text" name = "Lastname"></td></tr>
	<tr><td>Email Address</td></tr>
	<tr><td><input type="text" name = "Email" ></td></tr>
	<tr><td>Username</td></tr>
	<tr><td><input type="text" name = "Username" ></td></tr>
	<tr><td>Password</td></tr>
	<tr><td><input type="password" name = "Password" ></td></tr>
	<tr><td><input type="submit" value = "Sign Up"></td></tr>
	</table>
</form>
</body>
</html>