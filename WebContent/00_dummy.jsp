<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--  Note:: CSS includes are stored in the respective header files -->
<title>Bookings.Yeah!</title>
</head>
<body>

<p> Replace user_*_X.html with manager and owner as required. </p>

<%	if(user.getUserName() == null) {
	// user not logged in
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// user is logged in
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>
<p>A dummy jsp to use when creating new pages</p>
</body>
</html>