<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.enterprise.beans.*, java.util.*"%>
    <jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
    <jsp:useBean id="bookingSearchRes" class="com.enterprise.beans.BookingSearchBean" scope="session" />
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Bookings.Yeah! Manage Bookings</title>
<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script>
$(function() {
    $("#datepicker_in").datepicker();
    $("#datepicker_in").datepicker("option", "dateFormat", "dd/MM/yy");
    $("#datepicker_out").datepicker();
    $("#datepicker_out").datepicker("option", "dateFormat", "dd/MM/yy");
    
    $("#datepicker_in").val("${startdate}");
    $("#datepicker_out").val("${enddate}");
});

</script>
</head>
<body>
<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>

<div class="center">
        <form action='SearchBookings' method="post">
            <table>
                <tr>
                    <td>*Checkin Date:</td>
                    <td><input type="text" id="datepicker_in" name="checkin" value="${startdate}"></td>
                </tr>
                <tr>
                    <td>*Checkout Date:</td>
                    <td><input type="text" id="datepicker_out" name="checkout" value="${enddate}"></td>
                </tr>
                <tr>
                    <td colspan="2" align="left"><input type="submit" value="search"></td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>