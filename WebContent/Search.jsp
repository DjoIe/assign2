<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.enterprise.beans.*, java.util.*"%>
<jsp:useBean id="bookingSearchRes" class="com.enterprise.beans.BookingSearchBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/tr/html4/loose.dtd">

<jsp:useBean id="cart" class="com.enterprise.beans.CartBean" scope="session" />
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<jsp:useBean id="searchresult" class="com.enterprise.beans.SearchBean" scope="session" />

<html>
<head>
<title>Search</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">

<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script>
$(function() {
    $("#datepicker_in").datepicker();
    $("#datepicker_in").datepicker("option", "dateFormat", "dd/MM/yy");
    $("#datepicker_out").datepicker();
    $("#datepicker_out").datepicker("option", "dateFormat", "dd/MM/yy");
    
    $("#datepicker_in").val("${startdate}");
    $("#datepicker_out").val("${enddate}");
});

</script>
</head>
<body>
<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>


    <div class="center">
        <h2>Search to book rooms.</h2>
        <form action='SearchHotel' method="post">
            <table>
                <tr>
                    <td>*Checkin Date:</td>
                    <td><input type="text" id="datepicker_in" name="checkin" value="${startdate}"></td>
                </tr>
                <tr>
                    <td>*Checkout Date:</td>
                    <td><input type="text" id="datepicker_out" name="checkout" value="${enddate}"></td>
                </tr>
                <tr>
                    <td>*City:</td>
                    <td><input type="text" id="city" name="city" value="${city}"></td>
                </tr>
                <tr>
                    <td>*Number of Rooms:</td>
                    <td><input type="text" name="roomnum" value="${roomnum}"></td>
                </tr>
                <tr>
                    <td>Max price per night:</td>
                    <td><input type="text" name="maxprice"></td>
                </tr>
                <tr>
                    <td style="padding-top:20px" colspan="2" align="center"><input onclick="show_result()" type="submit"
                        value="search"></td>
                </tr>
            </table>
        </form>
        <br>
        <%
             String alert = (String) request.getAttribute("alert");
             if(alert == null){
            	 alert = "";
             }
         %>
        <a class="alert"><%=alert %></a>
    </div>
    
    <br>
    <hr>
    <br>
    
    
	<div class="center result">
	    <h3>Search Result:</h3>
        <form action='BookRooms' method="post">
	        <table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
	            <tr>
	                <th>Hotel</th>
	                <th>Room Type</th>
	                <th>Room number</th>
	                <th>Price</th>
	                <th>Period</th>    
	                <th>Extra bed</th>            
	                <th>Select</th>
	            </tr>
	    
               <%
	                RoomBean entry = null;
	                for (int i = 0; i < searchresult.getResult().size(); i++) {
	                    entry = (RoomBean) searchresult.getResult().get(i);
	            %>

		            <tr>
		                <td><%=entry.getHotel()%></td>
		                <td><%=entry.getRoomType()%></td>
		                <td><%=entry.getRoomNumAvailable()%></td>
		                <td><%=entry.getPrice()%></td>
		                <td><%=entry.getPeriod()%></td>
		                <%
		                if(entry.getRoomType().equals("single")){
		                %>
		                	<td><input disabled = "disabled" onchange="selectBed(this)" name="extrabed" type="checkbox" value="<%=i%>"></td>
		                <%
		                }else{
		                %>
		                	<td><input onchange="selectBed(this)" name="extrabed" type="checkbox" value="<%=i%>"></td>
		                <%
		                }
		                %>
		                <%-- <td><input onchange="selectBed(this)" name="extrabed" type="checkbox" value="<%=i%>"></td> --%>
		                <td><input onchange="selectRoom(this)" name="select" type="checkbox" value="<%=i%>"></td>
		            </tr>
	            
	            <%
	                }
	            %>
	        </table>
            
            <br>
	        <input type="submit"  value="book">
	        
	        <input name="startdate" type="hidden" value="${startdate}">
            <input name="enddate" type="hidden" value="${enddate}">
            <input name="roomnum" type="hidden" value="${roomnum}">
<!--             <input name="roomnum" type="hidden" value="1"> -->
            
            
        </form>
    </div>
    

    <p>


<script>
    
    function selectBed(bed){
        var select = $(bed).parent("td").next().children("input");
        if(bed.checked == true){
            $(select).prop('checked', true);
        }
    }
    
    function selectRoom(select){
    	var bed = $(select).parent("td").prev().children("input");
    	if(select.checked == false){
            $(bed).prop('checked', false);
        }
    }
    
<%
    String search = "";
    if(request.getAttribute("search") == null)
        search = "false";
    else
        search = (String)request.getAttribute("search");
        
    if(search.equals("false")){
    }else if(search.equals("true")){
%>
        $(".result").show();
<%
    }
%>
    
</script>

</html>