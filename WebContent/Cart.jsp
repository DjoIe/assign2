<%@page import="com.enterprise.beans.BookingBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.enterprise.beans.*, java.util.*, java.text.SimpleDateFormat, 
    java.text.DateFormat;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="cart" class="com.enterprise.beans.CartBean" scope="session" />
<jsp:useBean id="checkout" class="com.enterprise.beans.CartBean" scope="session" />
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bookings.Yeah! Cart</title>
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>

    <div class="center">
        <form action='Checkout' method="post">
            <table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
                <tr>
                    <th>Hotel</th>
                    <th>Room Type</th>
                    <th>Number of Rooms</th>
                    <th>Price</th>
                    <th>Period</th>                
                    <th>Extra bed</th>
                    <th>Checkin Date</th>
                    <th>Checkout Date</th>
                    <th></th>
                    <th></th>
                </tr>
        
                <%
                    DateFormat df = new SimpleDateFormat("dd/MMMM/yy");
                    BookingBean booking = null;
                    for (int i = 0; i < cart.getBookings().size(); i++) {
                        
                    	booking = (BookingBean) cart.getBookings().get(i);
                    	
                    	String startDate = df.format(booking.getStartDate());
                        String endDate = df.format(booking.getEndDate());
                %>
                    <tr>
                        <td><%=booking.getRoom().getHotel()%></td>
                        <td><%=booking.getRoom().getRoomType()%></td>
                        <td><%=booking.getRoomnum()%></td>
                        <td><%=booking.getRoom().getPrice()%></td>
                        <td><%=booking.getRoom().getPeriod()%></td>
                        
<%--                         <td><%=booking.GetExtrabed()%></td> --%>
                        
                        <%if(booking.getRoom().getRoomType().equals("single")){ %>
                            <td><input disabled = "disabled" onchange="selectBed(this)" name="extrabed" type="checkbox" value="<%=i%>"></td>
                        <%}else if(booking.GetExtrabed().equals("No")){ %>
                            <td><input onchange="selectBed(this, <%=i%>)" name="extrabed" type="checkbox" value="<%=i%>"></td>
                        <%}else if(booking.GetExtrabed().equals("Yes")){ %>
                            <td><input onchange="selectBed(this, <%=i%>)" name="extrabed" type="checkbox" value="<%=i%>" checked="checked"></td>
                        <%} %>
                        
                        <td><%=startDate%></td>
                        <td><%=endDate%></td>
                        <td><a href="CartDeleteBooking?id=<%=i%>">delete</a></td>
                        <td><input name="select" type="checkbox" value="<%=i%>"></td>
                    </tr>
                
                <%
                    }
                %>
            </table>
            
            <div style="padding:10px 0;">
                <a>Total Price: <%=cart.getTotalCost()%></a>
            </div>
            <br>
            <input type="submit"  value="Checkout">
        </form>
    </div>
</body>


<script>
	function selectBed(bed, id){
	    if(bed.checked == true){
	    	if (confirm("Update extra bed?")) {
	    	    document.location.href = "CartUpdate?extrabed=1&id="+id;
	    	}else{
	    		$(bed).prop('checked', false);
	    		
	    	}
	    }else{
	    	if (confirm("Update extra bed?")) {
	    		document.location.href = "CartUpdate?extrabed=0&id="+id;
	    	}else{
	    		$(bed).prop('checked', true);
	    	}
	    }
	}
</script>
</html>