<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.enterprise.beans.*, java.util.*"%>
    <jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
    <jsp:useBean id="bookingSearchRes" class="com.enterprise.beans.BookingSearchBean" scope="session" />
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Bookings.Yeah! Manage Bookings</title>


</head>
<body>
<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>

<%if (bookingSearchRes != null) { %>
	<div class="center">
        <form action="EditBookings" method="post">
	        <table width="100%" border=1 align="center">
	            <tr>
	    			<th>Edit</th>
	                <th>Hotel</th>
	                <th>Start Date</th>
	                <th>End Date</th>
	            </tr>
	    
               <%
	                BookingsBeans entry = null;
	                for (int i = 0; i < bookingSearchRes.getBookingRes().size(); i++) {
	                    entry = (BookingsBeans) bookingSearchRes.getBookingRes().get(i);
	                    //System.out.println("result size is: " + bookingSearchRes.getBookingRes().size());  
	            %>

		            <tr align="center">
		            	<td><input name="chkBooking" type="checkbox" value="<%=i%>"></td>
		                <td><%= entry.getHotelName() %></td>
		                <td><%= entry.getStartDateString() %></td>
		                <td><%= entry.getEndDateString() %></td>
		            </tr>
	           <%
	                }
               }
	           %> 
	            
	            <tr align="center"><td></td><td><input type="submit" name="act" value="Edit Selected Bookings"></td>
	            <td><input type="submit" name="act" value="Cancel Selected Bookings"></td></tr>
	            <tr align="center"><td><input type="submit" name="act" value="Clear Search"></td></tr>
	        </table>
        </form>
    </div>


</body>
</html>