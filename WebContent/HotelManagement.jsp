<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Hotel Management</title>

<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script>
$(function() {
    $("#datepicker_in").datepicker();
    $("#datepicker_in").datepicker("option", "dateFormat", "dd/MM/yy");
    $("#datepicker_out").datepicker();
    $("#datepicker_out").datepicker("option", "dateFormat", "dd/MM/yy");
    
    $("#datepicker_in").val("${startdate}");
    $("#datepicker_out").val("${enddate}");
});
</script>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="owner_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="owner_header_loggedin.html" %>
	<%
}
%>

<h1>CHAIN: <c:out value ="${chain.name}"/></h1>
<h2>OCCUPANCY</h2>
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<tr><td>Hotel</td><td>City</td><td>Vacant Rooms</td><td>Occupied Rooms</td>
<td>Edit Maintenance</td><td>Edit Discounts</td><td>Edit Peak Periods</td></tr>
<c:forEach var="h" items="${chain.hotels}">
 		<tr>
 			<td><c:out value="${h.name}" /></td>
 			<td><c:out value="${h.city}" /></td>
     		<td><c:out value="${h.numVacantRooms}" /></td>
     		<td><c:out value="${h.numOccupiedRooms}" /></td>
     		<td><form method = post action = Maintenance>
     		<input type = submit value = "Edit Maintenance">
     		<input type = hidden name = hid value = "${h.id}" >
     		</form></td>
     		<td><form method = post action = Discounts>
     		<input type = submit value = "Edit Discounts">
     		<input type = hidden name = hid value = "${h.id}" >
     		</form></td>
     		<td><form method = post action = Peaks>
     		<input type = submit value = "Edit Peak Periods">
     		<input type = hidden name = hid value = "${h.id}" >
     		</form></td>
     	</tr>
	</c:forEach>
</table>




</body>
</html>