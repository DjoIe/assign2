<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Manage Peaks</title>

<link rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<link rel="stylesheet" href="css/search.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script>
$(function() {
    $("#datepicker_in").datepicker();
    $("#datepicker_in").datepicker("option", "dateFormat", "dd/MM/yy");
    $("#datepicker_out").datepicker();
    $("#datepicker_out").datepicker("option", "dateFormat", "dd/MM/yy");
    
    $("#datepicker_in").val("${startdate}");
    $("#datepicker_out").val("${enddate}");
});
</script>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="owner_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="owner_header_loggedin.html" %>
	<%
}
%>
<table><tr><td><font color="#ff0000"><c:out value="${msg}" /></font></td></tr></table>
<h1>HOTEL: <c:out value="${hotel.name}" /></h1>
<h2>Peaks</h2>
<form method = post action = RemovePeak>
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
<c:if test="${not empty peaks}">
<tr><td>Hotel</td><td>Surcharge</td><td>Start Date</td><td>End Date</td><td>Remove Peak</td></tr>
<c:forEach var="p" items="${peaks}">
 		<tr>
 			<td><c:out value="${p.hotel}" /></td>
     		<td>%<c:out value="${p.surcharge}" /> surcharge</td>
     		<td><c:out value="${p.startDate}" /></td>
     		<td><c:out value="${p.endDate}" /></td>
     		<td><input type = "checkbox" name = "removePeak" value = "${p.id}"></td>
     	</tr>
</c:forEach>

	<tr><td><input type = "submit" name = "dbutton" value = "Remove Selected Peaks">
	<input type = "hidden" name = "hid" value = "${hotel.id}"></td></tr>

</c:if>
<c:if test="${empty peaks}">
	<tr><td>No peak periods for Hotel</td></tr>
</c:if>
</table>
</form>
<h2>Add New Peak Period</h2>
<form method = post action = AddPeak>
<table class="w3-table w3-bordered w3-striped w3-border" width="100%" border=1>
     	<tr><td>SURCHARGE: </td>
 			<td><input type= "text" name = "surcharge"></td>
 		<tr><td>START/END DATE: </td>
 			<td><input type="text" id="datepicker_in" name="peakStart" value="${startdate}"></td>
 			<td><input type="text" id="datepicker_out" name="peakEnd" value="${enddate}"></td></tr>
 			<tr><td><input type = "submit" value = "Add Peak Period">
     			<input type = "hidden" name = "hid" value = "${hotel.id}"></td></tr>
     	
</table>
</form>

</body>
</html>