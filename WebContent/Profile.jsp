<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<jsp:useBean id="bookingSearchRes" class="com.enterprise.beans.BookingSearchBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Bookings.Yeah! Profile</title>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>

	<H1>Profile</H1>
	<form method="post" name="Edit" action="EditProfile">
	<table>
	<tr><td><b>Username</b></td></tr>
	<tr><td><c:out value="${user.userName}" /></td></tr>
	<tr><td><b>Password</b></td></tr>
	<tr><td><input type="password" name = "password" value = "<c:out value="${user.password}" />"></td></tr>
	<tr><td><b>Nickname</b></td></tr>
	<tr><td><input type="text" name = "nickName" value = "<c:out value="${user.nickName}" />"></td></tr>
	<tr><td><b>First Name</b></td></tr>
	<tr><td><input type="text" name = "firstName" value = "<c:out value="${user.firstName}" />"></td></tr>
	<tr><td><b>Last Name</b></td></tr>
	<tr><td><input type="text" name = "lastName" value = "<c:out value="${user.lastName}" />"></td></tr>
	<tr><td><b>Email Address</b></td></tr>
	<tr><td><input type="text" name = "email" value = "<c:out value="${user.email}" />"></td></tr>
	<tr><td><b>Address</b></td></tr>
	<tr><td><input type="text" name = "address" value = "<c:out value="${user.address}" />"></td></tr>
	<tr><td><b>Credit Card</b></td></tr>
	<tr><td><input type="text" name = "creditCard" value = "<c:out value="${user.creditCard}" />"></td></tr>
	<tr><td><input type="submit" value = "Save Changes"></td></tr>
	</table>
	</form>
</body>
</html>