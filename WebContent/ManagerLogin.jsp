<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean"
	scope="session" />
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<title>Manager Login</title>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="manager_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="manager_header_loggedin.html" %>
	<%
}
%>
<table><tr><td><font color="#ff0000"><c:out value="${error}" /></font></td></tr></table>
<form method="post" name="Login" action="ManagerLogin">
	<h1>MANAGER LOGIN</h1>
	<table>
	<tr><td>Username</td></tr>
	<tr><td><input type="text" name = "Username"></td></tr>
	<tr><td>Password</td></tr>
	<tr><td><input type="password" name = "Password"></td></tr>
	<tr><td><input type="submit" value = "Login"></td></tr>
	</table>
</form>
</body>
</html>