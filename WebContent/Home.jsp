<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:useBean id="user" class="com.enterprise.beans.PersonBean" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">

<title>Bookings.Yeah!</title>
</head>
<body>

<% //if(user.getUserName().equalsIgnoreCase("")) {
	if(user.getUserName() == null) {
	// include user_header.html
	%>
	<%@ include file="user_header.html" %>
	<%
}
else {
	// include user_header_loggedin.html
	%>
	<%@ include file="user_header_loggedin.html" %>
	<%
}
%>

<div class="w3-content" style="max-width:800px;position:relative">

	<div class="myHotelSlide w3-xlarge w3-card-8 w3-center">
	  <p>hello world! </p>
	  <p> price info... </p>
	  <p>book now!</p>
	</div>
	<div class="myHotelSlide w3-xlarge w3-card-8 w3-center">
	  <p>Here is a list of hotels! </p>
	  <p> price info... </p>
	  <p>book now!</p>
	</div>
	<div class="myHotelSlide w3-xlarge w3-card-8 w3-center">
	  <p>Hilton Hotel </p>
	  <p> $100 </p>
	  <p>book now!</p>
	</div>
	<div class="myHotelSlide w3-xlarge w3-card-8 w3-center">
	  <p>Use SQL queries to populate this... </p>
	  <p> price info... </p>
	  <p>book now!</p>
	</div>
	<div class="myHotelSlide w3-xlarge w3-card-8 w3-center">
	  <p> Trump Palace </p>
	  <p> $1000 </p>
	  <p>book now!</p>
	</div>
	<div class="myHotelSlide w3-xlarge w3-card-8 w3-center">
	  <p>Uncle Bob's Happy Fun House </p>
	  <p> Free! </p>
	  <p>book now!</p>
	</div>

<a class="w3-btn-floating" style="position:absolute;top:45%;left:0" onclick="plusDivs(-1)">❮</a>
<a class="w3-btn-floating" style="position:absolute;top:45%;right:0" onclick="plusDivs(1)">❯</a>
</div>
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("myHotelSlide");
  if (n > x.length) {slideIndex = 1;}    
  if (n < 1) {slideIndex = x.length;}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>
<div class="w3-row-padding w3-center w3-margin-top"></div>
<div class="w3-container">
<div class="w3-accordion">
  <button onclick="myAccFunc('Demo1')" class="w3-padding-hor-16 w3-hover-dark-grey w3-btn-block w3-left-align">Why book with us?</button>
  <div id="Demo1" class="w3-accordion-content">
    <div class="w3-container">
		<p> We are a small business with an annual turnover in excess of $50 Billion, and have
		a customer satisfaction approaching 100%. In addition to this, our tax bill over the last
		decade has been $1.
		</p>
    </div>
  </div>
  <button onclick="myAccFunc('Demo2')" class="w3-padding-hor-16 w3-hover-dark-grey w3-btn-block w3-left-align">Open Section 2</button>
  <div id="Demo2" class="w3-accordion-content">
    <a class="w3-text-black w3-padding-hor-16" href="javascript:void(0)">Link 1</a>
    <a class="w3-text-black w3-padding-hor-16" href="javascript:void(0)">Link 2</a>
    <a class="w3-text-black w3-padding-hor-16" href="javascript:void(0)">Link 3</a>
  </div>
  <button onclick="myAccFunc('Demo3')" class="w3-padding-hor-16 w3-hover-dark-grey w3-btn-block w3-left-align">About Us</button>
  <div id="Demo3" class="w3-accordion-content">
    <p>Incorporated in the Cayman Islands by Mossack Fonseca</p>
  </div>
  
 </div>
</div>
<script>
//Accordions
function myAccFunc(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-dark-grey";
    } else { 
        x.className = x.className.replace(" w3-show", "");
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace(" w3-dark-grey", "");
    }
}
</script>
<%--   
<%@ include file="footer.html"--%>

</body>
</html>