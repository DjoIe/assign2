package com.enterprise.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.HotelBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.beans.RoomBean;
import com.enterprise.dao.BookingsDAO;
import com.enterprise.dao.HotelDAO;
import com.enterprise.dao.PeopleDAO;



@WebServlet("/Maintenance")
public class Maintenance extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Maintenance() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		//ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		HotelDAO hdao = new HotelDAO();
		int hid = Integer.parseInt(request.getParameter("hid"));
		//Rebuild hotel
		//user.setHotel(hdao.getHotel(user.getHotel().getId()));
		HotelBean hotel = hdao.getHotel(hid);
		request.setAttribute("hotel",hotel);
		String nextPage = "Maintenance.jsp";
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		
		rd.forward(request, response);
		
	}

}
