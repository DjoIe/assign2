package com.enterprise.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.BookingBean;
import com.enterprise.beans.CartBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.beans.SearchBean;



/**
 * Servlet implementation class BookRooms
 */
@WebServlet("/BookRooms")
public class BookRooms extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookRooms() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchBean searchResult = (SearchBean) request.getSession().getAttribute("searchresult");
		CartBean cart = (CartBean) request.getSession().getAttribute("cart");
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		
		if(cart == null){
			cart = new CartBean();
		}
		
		
		if(user != null)
			cart.setUserBean(user);
		
		String[] select = request.getParameterValues("select");
		String[] extrabed = null;
		if(request.getParameterValues("extrabed") != null){
			extrabed = request.getParameterValues("extrabed");
		}else{
			extrabed = new String[0];
		}
		
		String checkin = request.getParameter("startdate");
		String checkout = request.getParameter("enddate");
		String roomNum = request.getParameter("roomnum");
		int roomNumInt = 0;
		if(!roomNum.equals(""))
			roomNumInt = Integer.parseInt(roomNum);
				
		DateFormat format = new SimpleDateFormat("dd/MMMM/yy", Locale.ENGLISH);
		Date startDate = null;
		Date endDate = null;
		
		try {
			startDate = format.parse(checkin);
			endDate = format.parse(checkout);
			
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		
		String nextPage = "Search.jsp";
		String errorMsg = "";
		
		int index_i;
		int index_j;
		if(select != null){
			for(int i=0; i<select.length; i++){
				index_i = Integer.parseInt(select[i]);
				
				BookingBean booking = new BookingBean();
				booking.setBookee(user);
				booking.setRoom(searchResult.getResult().get(index_i));
				
				long diff = Math.abs(startDate.getTime() - endDate.getTime());
				int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
				
				booking.setStartDate(startDate);
				booking.setEndDate(endDate);
				
				booking.setRoomnum(roomNumInt);
				
				
				booking.setCost(booking.getRoom().getPrice()*roomNumInt*diffDays);
				
				String isExtraBed = "No";
				for(int j=0; j<extrabed.length; j++){
					index_j = Integer.parseInt(extrabed[j]);
					if(index_j == index_i){
						isExtraBed = "Yes";
						break;
					}
				}
				booking.setExtrabed(isExtraBed);
				
				cart.getBookings().add(booking);
					
			}
			
			nextPage = "Cart.jsp";
		}else{
			errorMsg = "Please select booking rooms";
		}

		// update totoal price
		int totalPrice = 0;
		for(int i=0; i<cart.getBookings().size(); i++){
			totalPrice += cart.getBookings().get(i).getCost();
			if(cart.getBookings().get(i).GetExtrabed().equals("Yes")){
				totalPrice += 35*cart.getBookings().get(i).getRoomnum();
			}
		}
		cart.setTotalCost(totalPrice);
		
		
		request.setAttribute("errorMsg", errorMsg);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
		
	}

}
