package com.enterprise.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.el.ELException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.ActivationBean;
import com.enterprise.dao.ActivationDAO;

/**
 * Servlet implementation class Activate
 */
@WebServlet("/Activate")
public class Activate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Activate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		int id = Integer.parseInt(request.getParameter("id"));
		String key = request.getParameter("link");
		RequestDispatcher rh=request.getRequestDispatcher("/user_header_loggedin.html");
		rh.include(request, response);
		ActivationDAO dao = new ActivationDAO();
		try {
			ActivationBean act = dao.getAcoount(id, key);
			if (act == null) { 
				out.print("YOUR ACTIVATION REQUEST COULD NOT BE FOUND");
			}else{
				if (act.getKey().equals(key)){
					dao.Activate(id);
					out.print("YOUR ACCOUNT HAS BEEN ACTIVATED SUCCESSFULLY!");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	//	out.print(id + key);
	//	RequestDispatcher rf=request.getRequestDispatcher("/footer.html");
	//	rf.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
