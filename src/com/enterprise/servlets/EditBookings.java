package com.enterprise.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.BookingSearchBean;
import com.enterprise.beans.BookingsBeans;
import com.enterprise.dao.BookingsDAO;


@WebServlet("/EditBookings")

public class EditBookings extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	public EditBookings() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextPage = "Home.jsp";
		BookingSearchBean bookingResult = (BookingSearchBean) request.getSession().getAttribute("bookingSearchRes");
		ArrayList<BookingsBeans> bookingList = bookingResult.getBookingRes();
		ArrayList<BookingsBeans> newBookings = new ArrayList<BookingsBeans>(); 
		BookingsBeans currBooking;
		int idx;
		String checkEditBooking[] = request.getParameterValues("chkBooking");
		System.out.println("editing servlet");
		String act = request.getParameter("act");
		java.util.Date emptyDate = new java.util.Date(0);
		
		if (act.equals("Edit Selected Bookings")){
			System.out.println("editing booking");
			BookingsDAO dao = new BookingsDAO();
			int newPrice = 0;
			for(int i = 0; i < checkEditBooking.length; i++) {
				idx = Integer.parseInt(checkEditBooking[i]);
				currBooking = bookingList.get(idx);
				currBooking.setCancelStatus(false);
				newPrice = dao.updatePrice(currBooking);
				newBookings.add(currBooking);
			}
			nextPage = "EditSelectedBookings.jsp";
			bookingResult.setBookingsToEdit(newBookings);
			request.getSession().setAttribute("bookingSearchres", bookingResult);
		}
		
		if (act.equals("Cancel Selected Bookings")){
			System.out.println("cancel booking");
			//BookingsDAO bookingDao = new BookingsDAO();
			BookingsBeans emptyBooking = new BookingsBeans();
			emptyBooking.setHotelName("Cancel Booking");
			
			for(int i = 0; i < checkEditBooking.length; i++) {
				idx = Integer.parseInt(checkEditBooking[i]);
				currBooking = bookingList.get(idx);
				currBooking.setCancelStatus(true);
				currBooking.setStartDate(emptyDate);
				currBooking.setEndDate(emptyDate);
				currBooking.setCost(0);
				
				//bookingDao.DeleteBooking(currBooking.getID());
				newBookings.add(currBooking);
			}
			nextPage = "ConfirmBookingChange.jsp";
			bookingResult.setUpdatedBookings(newBookings);
			request.getSession().setAttribute("bookingSearchres", bookingResult);
		}
		
		if (act.equals("Clear Search")) {
			request.getSession().removeAttribute("bookingSearchres");
			nextPage = "CancelBookingChanges.jsp";
		}
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}
}
