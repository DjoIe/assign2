package com.enterprise.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.BookingSearchBean;
import com.enterprise.beans.BookingsBeans;
import com.enterprise.dao.BookingsDAO;

@WebServlet("/UpdateBooking")

public class UpdateBooking extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	public UpdateBooking() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookingSearchBean bookingResult = (BookingSearchBean) request.getSession().getAttribute("bookingSearchRes");
		ArrayList<BookingsBeans> changedBookings = bookingResult.getUpdatedBookings();
		BookingsBeans currBooking = null;
		BookingsDAO bDAO = new BookingsDAO();
		//String nextPage = "Profile.jsp";
		
		/**
		 * check whether user has cnacellled or not.
		 */
		
		String act = request.getParameter("act");
		String nextPage = "";
		
		if (act.equals("Confirm booking change")){  
			// put the modified booking into the database
			for(int i = 0; i < changedBookings.size(); i++) {
				currBooking = changedBookings.get(i);
				if(currBooking.getCancelStatus()) {
					// cancel the booking
					bDAO.DeleteBooking(currBooking.getID());
				}
				else {
					try {
						bDAO.updateBooking(currBooking);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			nextPage = "Profile.jsp";
		}
		else {
			nextPage = "CancelBookingChanges.jsp";
		}
		request.getSession().removeAttribute("bookingSearchres");  // clear the search object
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}

}
