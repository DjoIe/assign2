package com.enterprise.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.BookingSearchBean;
import com.enterprise.beans.BookingsBeans;
import com.enterprise.dao.*;

@WebServlet("/ModifyBookings")
public class ModifyBookings extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ModifyBookings() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookingSearchBean bookingResult = (BookingSearchBean) request.getSession().getAttribute("bookingSearchRes");
		
		ArrayList<BookingsBeans> updatedBookings = bookingResult.getBookingsToEdit();  // dump everything into updated bookings then process
		ArrayList<BookingsBeans> errBookings = new ArrayList<BookingsBeans>();
		BookingsBeans currBooking = null;
		String tmp = "";
		java.util.Date tmpDate;
		String newStartDates[] = request.getParameterValues("startDate");
		String newEndDates[] = request.getParameterValues("endDate");
		String newExtraBeds[] = request.getParameterValues("extraBed");
		DateFormat format = new SimpleDateFormat("dd/MMMM/yy", Locale.ENGLISH);
		
		BookingsDAO dao = new BookingsDAO();
		int somePrice;
		String act = request.getParameter("act");
		String nextPage = "";
		
		if (act.equals("Update Bookings")){ 
			System.out.println("we're off to update bookings!");

			for(int i = 0; i < newStartDates.length; i++) {
				currBooking = updatedBookings.get(i);
				tmp = newStartDates[i];
				try {
					tmpDate = format.parse(tmp);
					currBooking.setStartDate(tmpDate);
					updatedBookings.set(i, currBooking);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			for(int i = 0; i < newEndDates.length; i++) {
				currBooking = updatedBookings.get(i);
				tmp = newEndDates[i];
				try {
					tmpDate = format.parse(tmp);
					currBooking.setEndDate(tmpDate);
					updatedBookings.set(i, currBooking);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			for(int i = 0; i < newExtraBeds.length; i++) {
				currBooking = updatedBookings.get(i);
				tmp = newExtraBeds[i];
				if (tmp.equals("Yes")) {
					currBooking.setExtraBed(true);
				}
				else {
					currBooking.setExtraBed(false);
				}
				updatedBookings.set(i, currBooking);
			}
			
			for(BookingsBeans foo : updatedBookings) {
				
				somePrice = dao.updatePrice(foo);
				foo.setCost(somePrice);
				System.out.println("the price is: " + somePrice);
				/*
				if (dao.checkAvail(foo)) {
					somePrice = dao.updatePrice(foo);
					foo.setCost(somePrice);
				}
				else {
					//updatedBookings.remove(foo);
					errBookings.add(foo);
					System.out.println("adding booking to error");
				}
				*/
			}
			
			for(BookingsBeans foo : errBookings) {
				for(BookingsBeans bar : updatedBookings) {
					if(foo.equals(bar)) {
						updatedBookings.remove(bar);
						break;
					}
				}
			}
			
			if (updatedBookings.isEmpty()) {
				nextPage = "UpdateErr.jsp";
			}
			else {
				nextPage = "ConfirmBookingChange.jsp";
				bookingResult.setUpdatedBookings(updatedBookings);
			}
			
			request.getSession().setAttribute("bookingSearchres", bookingResult);
			
		}
		else {
			// cancel all changes
			nextPage = "CancelBookingChanges.jsp";
			request.getSession().removeAttribute("bookingSearchRes");
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}
}
