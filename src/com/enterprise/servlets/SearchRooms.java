package com.enterprise.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.PersonBean;
import com.enterprise.beans.RoomBean;
import com.enterprise.beans.SearchBean;
import com.enterprise.dao.SearchDAOImpl;

/**
 * Servlet implementation class SearchHotel
 */
@WebServlet("/SearchHotel")
public class SearchRooms extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchRooms() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchBean searchResult = (SearchBean) request.getSession().getAttribute("searchresult");
		
		int pass = 1;
		String nextPage = "Search.jsp";
		String city = "";
		String roomnum = "";
		int maxprice_int;
		
		String checkin = request.getParameter("checkin");
		String checkout = request.getParameter("checkout");
		
		city = request.getParameter("city");
		if(city == ""){
			request.setAttribute("alert", "Please input city");
			request.setAttribute("search", "false");
			pass = 0;
		}
		
		roomnum = request.getParameter("roomnum");
		if(roomnum == ""){
			request.setAttribute("alert", "Please input number of room");
			request.setAttribute("search", "false");
			pass = 0;
		}
		
		DateFormat format = new SimpleDateFormat("dd/MMMM/yy", Locale.ENGLISH);
		Date checkin_date = null;
		Date checkout_date = null;
		
		if(checkin != "" && checkout != "" && 
				checkin != null && checkout != null){
			try {
//				System.out.println(checkin);
				checkin_date = format.parse(checkin);
				checkout_date = format.parse(checkout);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		// input date
		if(checkin_date == null || checkout_date == null){
			request.setAttribute("alert", "Please input date");
			request.setAttribute("search", "false");
			pass = 0;
		}else if(checkout_date.compareTo(checkin_date) <= 0){
				request.setAttribute("alert", "Checkin has to be earlier than checkout");
				request.setAttribute("search", "false");
				pass = 0;
		}
		
		// max price
		String maxprice = request.getParameter("maxprice");
		if(maxprice != "" && maxprice != null)
			maxprice_int = Integer.parseInt(maxprice);
		else
			maxprice_int = 100000;
		
		if(pass == 1){
			SearchDAOImpl searchDAO = new SearchDAOImpl();
	 		try {
	 			
				List<RoomBean> rooms = searchDAO.searchHotel(
						checkin_date, 
						checkout_date, 
						city,
						Integer.parseInt(roomnum),
						maxprice_int
				);
				
				
				searchResult.setResult(rooms);
				
				// test
//				searchResult.setResult(new ArrayList<RoomBean>());
//				
//				RoomBean room1 = new RoomBean();
//				room1.setId(3);
//				room1.setHotel("Daya");
//				room1.setRoomType("single room");
//				room1.setPeriod("peak");
//				room1.setPrice(120);
//				room1.setRoomNumAvailable(12);
//				searchResult.getResult().add(room1);
//				
//				RoomBean room2 = new RoomBean();
//				room2.setId(3);
//				room2.setHotel("Queen");
//				room2.setRoomType("Double room");
//				room2.setPeriod("off-peak");
//				room2.setPrice(200);
//				room2.setRoomNumAvailable(23);
//				searchResult.getResult().add(room2);
				
//				request.setAttribute("searchResult", searchResult);
				
				if(searchResult.getResult().size() > 0){
					request.setAttribute("search", "true");
				}else{
					request.setAttribute("alert", "Can not find a room, try another search");
					request.setAttribute("search", "false");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		request.setAttribute("startdate", checkin);
		request.setAttribute("enddate", checkout);
		request.setAttribute("city", city);
		request.setAttribute("roomnum", roomnum);
		
		
		PersonBean user = (PersonBean)request.getSession().getAttribute("user");
		if(user.getUserName() == null){
			request.setAttribute("alert", "Please login to search");
			request.setAttribute("search", "false");
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}

}
