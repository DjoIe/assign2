package com.enterprise.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.CartBean;

/**
 * Servlet implementation class CartDeleteBooking
 */
@WebServlet("/CartDeleteBooking")
public class CartDeleteBooking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartDeleteBooking() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CartBean cart = (CartBean) request.getSession().getAttribute("cart");
		String nextPage = "Cart.jsp";
		
		int deleteId = 0;
		
		if(request.getParameter("id") != null){
			deleteId = Integer.parseInt(request.getParameter("id"));
			cart.getBookings().remove(deleteId);
		}
		
		// update totoal price
		int totalPrice = 0;
		for(int i=0; i<cart.getBookings().size(); i++){
			totalPrice += cart.getBookings().get(i).getCost();
			if(cart.getBookings().get(i).GetExtrabed().equals("Yes")){
				totalPrice += 35;
			}
		}
		cart.setTotalCost(totalPrice);
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
