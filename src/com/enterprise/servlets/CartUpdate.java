package com.enterprise.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.CartBean;

/**
 * Servlet implementation class CartUpdate
 */
@WebServlet("/CartUpdate")
public class CartUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CartBean cart = (CartBean) request.getSession().getAttribute("cart");
		String nextPage = "Cart.jsp";
		
		int index = 0;
		int extra_bed = 0;
		
		if(request.getParameter("id") != null &&
				request.getParameter("extrabed") != null){
			index = Integer.parseInt(request.getParameter("id"));
			extra_bed = Integer.parseInt(request.getParameter("extrabed"));
			
			if(extra_bed == 1){
				cart.getBookings().get(index).setExtrabed("Yes");
			}else if(extra_bed == 0){
				cart.getBookings().get(index).setExtrabed("No");
			}
		}
		
		
		// update totoal price
		int totalPrice = 0;
		for(int i=0; i<cart.getBookings().size(); i++){
			totalPrice += cart.getBookings().get(i).getCost();
			if(cart.getBookings().get(i).GetExtrabed().equals("Yes")){
				totalPrice += 35;
			}
		}
		cart.setTotalCost(totalPrice);
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
