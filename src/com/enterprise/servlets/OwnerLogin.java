package com.enterprise.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.ChainBean;
import com.enterprise.beans.HotelBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.dao.HotelDAO;
import com.enterprise.dao.PeopleDAO;


/**
 * Servlet implementation class Login
 */
@WebServlet("/OwnerLogin")
public class OwnerLogin extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OwnerLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		int owner = 1;
		String u = request.getParameter("Username");
		String p = request.getParameter("Password");
		System.out.println(u);
		System.out.println(p);
		String nextPage = "OwnerLogin.jsp";
		PeopleDAO dao = new PeopleDAO();
		HotelDAO hdao = new HotelDAO();
		String password = dao.GetPassword(u);
		System.out.println("PASSWORD SHOULD BE: "+password);
		if(p.equals(password)){
			
			PersonBean pb = dao.GetPerson(u);
			System.out.println("ROLE: "+pb.getRole());
			if(pb.getRole()!= owner){
				request.setAttribute("error","You do not have the appropriate access level");
			}
			else{
				user.copyDataFrom(pb);
				System.out.println("MANAGER ID IS: "+pb.getId());
				//HotelBean hotel = hdao.getHotelFromManager(pb.getId());//new HotelBean();
				ChainBean chain = hdao.getChainFromOwner(pb.getId());
				request.setAttribute("chain",chain);
				user.setChain(chain);
				nextPage = "HotelManagement.jsp";
			}
			//System.out.println("USER: "+user.getUserName()+", "+user.getEmail());
		}
		else{
			request.setAttribute("error","Incorrect Username/Password");
		}
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
		
	}

}
