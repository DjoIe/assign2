package com.enterprise.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.DiscountBean;
import com.enterprise.beans.HotelBean;
import com.enterprise.beans.PeakBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.beans.RoomBean;
import com.enterprise.dao.BookingsDAO;
import com.enterprise.dao.HotelDAO;
import com.enterprise.dao.PeopleDAO;



@WebServlet("/AddPeak")
public class AddPeak extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPeak() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		//ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		HotelDAO hdao = new HotelDAO();
		int hid = Integer.parseInt(request.getParameter("hid"));
		System.out.println("Peak START: "+request.getParameter("peakStart"));
		//DateFormat d = new DateFormat();
		
		int surcharge= Integer.parseInt(request.getParameter("surcharge"));
		
		Date start = null;
		Date end = null;
		DateFormat format = new SimpleDateFormat("dd/MMMM/yy", Locale.ENGLISH);
		try {
			start = format.parse(request.getParameter("peakStart"));
			end = format.parse(request.getParameter("peakEnd"));
			
			if(start.after(end)){
				System.out.println("Invalid dates");
				request.setAttribute("msg","Invalid Peak: Start date must be after end date");
			}
			else{
				
				//String rid = request.getParameter("RoomId");
				//hdao.updateRoomNum(Integer.parseInt(bid), Integer.parseInt(rid));
				hdao.createPeak(hid, surcharge, start, end);
				request.setAttribute("msg","Peak added successfully");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Date end = request.getParameter("discountEnd");
		
		//Rebuild hotel
		//user.setHotel(hdao.getHotel(user.getHotel().getId()));
		HotelBean hotel = hdao.getHotel(hid);
		request.setAttribute("hotel",hotel);
		String nextPage = "Peaks.jsp";
		ArrayList<PeakBean> peaks = hdao.getPeaks(hid);
		request.setAttribute("peaks",peaks);
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		
		rd.forward(request, response);
		
	}

}
