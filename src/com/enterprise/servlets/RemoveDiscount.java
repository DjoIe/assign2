package com.enterprise.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.DiscountBean;
import com.enterprise.beans.HotelBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.beans.RoomBean;
import com.enterprise.dao.BookingsDAO;
import com.enterprise.dao.HotelDAO;
import com.enterprise.dao.PeopleDAO;



@WebServlet("/RemoveDiscount")
public class RemoveDiscount extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveDiscount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		//ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		HotelDAO hdao = new HotelDAO();
		int hid = Integer.parseInt(request.getParameter("hid"));
		
		//DateFormat d = new DateFormat();
		
		String discountList[] = request.getParameterValues("removeDiscount");
		//String rid = request.getParameter("RoomId");
		//hdao.updateRoomNum(Integer.parseInt(bid), Integer.parseInt(rid));
		for(int i = 0; i < discountList.length; i++){
			hdao.deleteDiscount(Integer.parseInt(discountList[i]));
		}
		
	
		//Date end = request.getParameter("discountEnd");
		
		//Rebuild hotel
		//user.setHotel(hdao.getHotel(user.getHotel().getId()));
		HotelBean hotel = hdao.getHotel(hid);
		request.setAttribute("hotel",hotel);
		String nextPage = "Discounts.jsp";
		ArrayList<DiscountBean> discounts = hdao.getDiscounts(hid);
		request.setAttribute("discounts",discounts);
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		
		rd.forward(request, response);
		
	}

}
