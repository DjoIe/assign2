package com.enterprise.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
//import java.sql.Date;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.BookingSearchBean;
import com.enterprise.beans.BookingsBeans;
import com.enterprise.beans.PersonBean;
import com.enterprise.beans.RoomBean;
import com.enterprise.beans.SearchBean;
import com.enterprise.dao.BookingsDAO;
import com.enterprise.dao.SearchDAOImpl;

@WebServlet("/SearchBookings")
public class SearchBookings extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public SearchBookings() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Do get is called");
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookingSearchBean bookingResult = (BookingSearchBean) request.getSession().getAttribute("bookingSearchRes");
		
		if (bookingResult == null) {
			bookingResult = new BookingSearchBean();
		}
		
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		
		String nextPage;
		BookingsDAO bookingDao = new BookingsDAO();
		
		
			nextPage = "ManageBooking.jsp";
			System.out.println("doPost is called");
			//String checkin = request.getParameter("checkin");
			//String checkout = request.getParameter("checkout");
			//if(checkin != null && checkout != null){
				/*
				DateFormat format = new SimpleDateFormat("dd/MMMM/yy", Locale.ENGLISH);
				
				java.util.Date checkin_date = null;
				java.util.Date checkout_date = null;
				
				try {
					checkin_date = format.parse(checkin);
					checkout_date = format.parse(checkout);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				//java.sql.Date checkinSQLDate = new java.sql.Date(checkin_date.getTime());
				//java.sql.Date checkoutSQLDate = new java.sql.Date(checkout_date.getTime());
				 
				 */
				//ArrayList<BookingsBeans> bookingRes = null;
				ArrayList<BookingsBeans> bookingRes = bookingDao.GetAllUserBookings(user.getId());
						
						//.GetUserBookings(user.getId(), checkinSQLDate, checkoutSQLDate);		
				System.out.println("Num booking res: " + bookingRes.size());
				bookingResult.setBookingRes(bookingRes);
				 
		request.getSession().setAttribute("bookingSearchres", bookingResult);
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
		
	}
	
}