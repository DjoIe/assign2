package com.enterprise.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.HotelBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.beans.RoomBean;
import com.enterprise.dao.BookingsDAO;
import com.enterprise.dao.HotelDAO;
import com.enterprise.dao.PeopleDAO;


/**
 * Servlet implementation class Login
 */
@WebServlet("/RoomList")
public class RoomList extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoomList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		//ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		HotelDAO hdao = new HotelDAO();
		
		String bid = request.getParameter("bookingToAssign");
		String roomType = request.getParameter("roomType");
		System.out.println("BOOKING: "+bid);
		PersonBean guest = hdao.GetPersonFromBooking(Integer.parseInt(bid));
		
		request.setAttribute("guest", guest);
		for(int i = 0; i < user.getHotel().getVacantRooms().size();i++){
			System.out.println("i");
			System.out.println(user.getHotel().getVacantRooms().get(i).getRoomNumAvailable());
			System.out.println(user.getHotel().getVacantRooms().get(i).getRoomType());
		}
		request.setAttribute("roomType",roomType);
		request.setAttribute("bid",bid);
		request.setAttribute("hotel",user.getHotel());
		String nextPage = "RoomList.jsp";
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
		
	}

}
