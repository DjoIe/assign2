package com.enterprise.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.PersonBean;
import com.enterprise.dao.PeopleDAO;


/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		String u = request.getParameter("Username");
		String p = request.getParameter("Password");
		System.out.println(u);
		System.out.println(p);
		String nextPage = "Login.jsp";
		PeopleDAO dao = new PeopleDAO();
		String password = dao.GetPassword(u);
		if(p.equals(password)){
			nextPage = "Profile.jsp";
			PersonBean pb = dao.GetPerson(u);
			user.copyDataFrom(pb);
			//user = dao.GetPerson(u);
			/*user.setUserName(pb.getUserName());
			user.setEmail(pb.getEmail());
			user.setFirstName(pb.getFirstName());
			user.setLastName(pb.getLastName());
			user.setPassword(pb.getPassword());
			user.setRole(pb.getRole());
			user.setNickName(pb.getNickName());
			user.setAddress(pb.getAddress());
			user.setCreditCard(pb.getCreditCard());
			user.setId(pb.getId());*/

			System.out.println("USER: "+user.getUserName()+", "+user.getEmail());
		}
		else{
			request.setAttribute("error","Incorrect Username/Password");
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
		
	}

}
