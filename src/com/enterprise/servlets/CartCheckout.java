package com.enterprise.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.BookingBean;
import com.enterprise.beans.CartBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.dao.BookingsDAO;

/**
 * Servlet implementation class Checkout
 */
@WebServlet("/Checkout")
public class CartCheckout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartCheckout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CartBean cart = (CartBean) request.getSession().getAttribute("cart");
		CartBean checkout = (CartBean) request.getSession().getAttribute("checkout");
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		
		if(checkout == null){
			checkout = new CartBean();
		}
		
		String nextPage = "Checkout.jsp";
		String[] select = request.getParameterValues("select");
		
		int index_i;
		checkout.setBookings(new ArrayList<BookingBean>());
		if(select != null){
			for(int i=0; i<select.length; i++){
				index_i = Integer.parseInt(select[i]);
				checkout.getBookings().add(cart.getBookings().get(index_i));
			}
		}
		
		
		// update totoal price
		int totalPrice = 0;
		for(int i=0; i<checkout.getBookings().size(); i++){
			totalPrice += checkout.getBookings().get(i).getCost();
			if(checkout.getBookings().get(i).GetExtrabed().equals("Yes")){
				totalPrice += 35*checkout.getBookings().get(i).getRoomnum();
			}
		}
		checkout.setTotalCost(totalPrice);
		
		checkout.setUserBean(user);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
