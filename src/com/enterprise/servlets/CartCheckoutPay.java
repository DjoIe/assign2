package com.enterprise.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.BookingBean;
import com.enterprise.beans.CartBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.dao.BookingsDAO;
import com.enterprise.dao.PeopleDAO;

/**
 * Servlet implementation class CartCheckoutPay
 */
@WebServlet("/CartCheckoutPay")
public class CartCheckoutPay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartCheckoutPay() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CartBean checkout = (CartBean) request.getSession().getAttribute("checkout");
		CartBean cart = (CartBean) request.getSession().getAttribute("cart");
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		if(checkout == null){
			checkout = new CartBean();
		}
		if(cart == null){
			cart = new CartBean();
		}
		
		int pass = 1;
		String creditcard = "";
		String firstname = "";
		String lastname = "";
		String address = "";
		String email = "";
		
		creditcard = request.getParameter("creditcard");
		if(creditcard == ""){
			request.setAttribute("alert", "Please input Credit Card");
			pass = 0;
		}
		
		firstname = request.getParameter("firstname");
		if(firstname == ""){
			request.setAttribute("alert", "Please input first name");
			pass = 0;
		}
		
		lastname = request.getParameter("lastname");
		if(lastname == ""){
			request.setAttribute("alert", "Please input last name");
			pass = 0;
		}
		
		address = request.getParameter("address");
		if(address == ""){
			request.setAttribute("alert", "Please input address");
			pass = 0;
		}
		
		email = request.getParameter("email");
		if(email == ""){
			request.setAttribute("alert", "Please input email");
			pass = 0;
		}
		
		String nextPage = "Checkout.jsp";
		
		if(pass == 1){
			// update payment info 
			PeopleDAO peopleDAO = new PeopleDAO();
			user.setCreditCard(creditcard);
			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setAddress(address);
			user.setEmail(email);
			
			peopleDAO.updatePerson(user);
		
			// insert booking into bookings table
			BookingsDAO bookingsDAO = new BookingsDAO();
			for(int i=0; i<checkout.getBookings().size(); i++){
				try {
					bookingsDAO.addBooking(checkout.getBookings().get(i));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			// clear cart and checkout
			cart.setBookings(new ArrayList<BookingBean>());
			checkout.setBookings(new ArrayList<BookingBean>());
			cart.setTotalCost(0);
			checkout.setTotalCost(0);
			
			// go to unique url
			nextPage = "SearchBookings";
		}
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}

}
