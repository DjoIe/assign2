package com.enterprise.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.AdBeans;
import com.enterprise.dao.AdvertDAO;

@WebServlet("/Home")
public class MainLoad extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		ArrayList<AdBeans> dispList = new ArrayList<AdBeans>();
		AdvertDAO dao = new AdvertDAO();
		ArrayList<AdBeans> allRooms = null;
		try {
			allRooms = dao.GetHotelAdvert();
			Collections.shuffle(allRooms);
			int numSyd = 0, numMel = 0, numBris = 0, numAdl = 0, numPer = 0, numHob = 0;
			// sydney list
			
			for (AdBeans foo : allRooms) {
				if (foo.gethotelCity().equalsIgnoreCase("sydney")) {
					dispList.add(foo);
					++numSyd;
				}
				if (numSyd >= 2) {
					break;
				}
			}
			
			for (AdBeans foo : allRooms) {
				if (foo.gethotelCity().equalsIgnoreCase("melbourne")) {
					dispList.add(foo);
					++numMel;
				}
				if (numMel >= 2) {
					break;
				}
			}
			
			for (AdBeans foo : allRooms) {
				if (foo.gethotelCity().equalsIgnoreCase("brisbane")) {
					dispList.add(foo);
					++numBris;
				}
				if (numBris >= 2) {
					break;
				}
			}
			
			for (AdBeans foo : allRooms) {
				if (foo.gethotelCity().equalsIgnoreCase("adelaide")) {
					dispList.add(foo);
					++numAdl;
				}
				if (numAdl >= 2) {
					break;
				}
			}
			
			for (AdBeans foo : allRooms) {
				if (foo.gethotelCity().equalsIgnoreCase("perth")) {
					dispList.add(foo);
					++numPer;
				}
				if (numPer >= 1) {
					break;
				}
			}
			
			for (AdBeans foo : allRooms) {
				if (foo.gethotelCity().equalsIgnoreCase("hobart")) {
					dispList.add(foo);
					++numHob;
				}
				if (numHob >= 1) {
					break;
				}
			}
			
			Collections.shuffle(dispList);
			request.setAttribute("adlist", dispList);
			String nextPage = "Home_1.jsp";
			RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
			rd.forward(request, response);
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
