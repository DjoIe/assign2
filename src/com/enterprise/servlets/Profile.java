package com.enterprise.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.PersonBean;
import com.enterprise.dao.PeopleDAO;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/EditProfile")
public class Profile extends HttpServlet {
	
	//String userName,nickName,firstName,lastName,email,address,creditCard;
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//firstName = "David";lastName = "Smith";userName = "user1";nickName="Dave";email="email@email.com";address="1 somestreet somesuburb";creditCard="111-111-111";
		String nextPage = "Profile.jsp";
		PeopleDAO dao = new PeopleDAO();
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		PersonBean person = new PersonBean();
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		//String username = request.getParameter("Username");
		String password = request.getParameter("password");
		String address = request.getParameter("address");
		String creditCard = request.getParameter("creditCard");
		String nickName = request.getParameter("nickName");
		
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		//user.setUserName(username);
		user.setNickName(nickName);
		user.setAddress(address);
		user.setCreditCard(creditCard);
		user.setPassword(password);
		
		dao.updatePerson(user);
		
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	
	}

}
