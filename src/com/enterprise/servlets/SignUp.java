package com.enterprise.servlets;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.PersonBean;
import com.enterprise.dao.PeopleDAO;


/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PeopleDAO dao = new PeopleDAO();
	//	PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		PersonBean person = new PersonBean();
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		String nextPage = "Profile.jsp";
		String firstName = request.getParameter("Firstname");
		String lastName = request.getParameter("Lastname");
		String email = request.getParameter("Email");
		String username = request.getParameter("Username");
		String password = request.getParameter("Password");
		
		if(firstName == null||lastName == null || username == null || password == null || email == null){//||lastName.equals("")||email.equals("")||username.equals("")||password.equals("")){
			nextPage = "Login.jsp";
			request.setAttribute("error", "One or more required fields are blank");
		}
		else if(dao.usernameExists(username)){
			nextPage = "Login.jsp";
			request.setAttribute("error", "Username already exists");
		}
		else{
			person.setFirstName(firstName);
			person.setLastName(lastName);
			person.setEmail(email);
			person.setUserName(username);
			person.setPassword(password);
			dao.createPerson(person);
			user.copyDataFrom(person);
		//user = person;
		}
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		rd.forward(request, response);
	}

}
