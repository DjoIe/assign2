package com.enterprise.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.DiscountBean;
import com.enterprise.beans.HotelBean;
import com.enterprise.beans.PersonBean;
import com.enterprise.beans.RoomBean;
import com.enterprise.dao.BookingsDAO;
import com.enterprise.dao.HotelDAO;
import com.enterprise.dao.PeopleDAO;


/**
 * Servlet implementation class Login
 */
@WebServlet("/AddDiscount")
public class AddDiscount extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddDiscount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersonBean user = (PersonBean) request.getSession().getAttribute("user");
		//ArrayList<RoomBean> roomList = new ArrayList<RoomBean>();
		HotelDAO hdao = new HotelDAO();
		int hid = Integer.parseInt(request.getParameter("hid"));
		System.out.println("DISCOUNT START: "+request.getParameter("discountStart"));
		//DateFormat d = new DateFormat();
		int roomType = Integer.parseInt(request.getParameter("roomType"));
		int val= Integer.parseInt(request.getParameter("discount"));
		
		Date start = null;
		Date end = null;
		DateFormat format = new SimpleDateFormat("dd/MMMM/yy", Locale.ENGLISH);
		try {
			//start = new SimpleDateFormat("dd/MMMMMMMMMM/yyyy").parse(request.getParameter("discountStart"));
			//end = new SimpleDateFormat("dd/MMMMMMMMMM/yyyy").parse(request.getParameter("discountEnd"));
			start = format.parse(request.getParameter("discountStart"));//request.getParameter("discountStart");
			end = format.parse(request.getParameter("discountEnd"));
			if(start.after(end)){
				System.out.println("Invalid dates");
				request.setAttribute("msg","Invalid discount: Start date must be after end date");
			}
			else{
				System.out.println("ADDING DISCOUNT: ");
				//String rid = request.getParameter("RoomId");
				//hdao.updateRoomNum(Integer.parseInt(bid), Integer.parseInt(rid));
				hdao.createDiscount(hid, roomType, val, start, end);
				request.setAttribute("msg","Discount added successfully");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Date end = request.getParameter("discountEnd");
		
		//Rebuild hotel
		//user.setHotel(hdao.getHotel(user.getHotel().getId()));
		System.out.println("Rebuilding hotel "+hid);
		HotelBean hotel = hdao.getHotel(hid);
		System.out.println("rebuild done");
		request.setAttribute("hotel",hotel);
		String nextPage = "Discounts.jsp";
		ArrayList<DiscountBean> discounts = hdao.getDiscounts(hid);
		request.setAttribute("discounts",discounts);
		RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		
		rd.forward(request, response);
		
	}

}
