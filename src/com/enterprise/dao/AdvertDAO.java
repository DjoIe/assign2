package com.enterprise.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enterprise.beans.AdBeans;
import com.enterprise.common.DBConnectionFactory;
import com.enterprise.common.ServiceLocatorException;

public class AdvertDAO {
	
	private DBConnectionFactory services;
	
	public AdvertDAO() {
		try {
			services = new DBConnectionFactory();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}
	
	public void getHotelDeets(AdBeans someAd) throws Exception {
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from hotels where id = ?");
			stmt.setInt(1, someAd.getHotelID());
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				someAd.sethotelCity(rs.getString("city"));
				someAd.setHotelName(rs.getString("name"));
			}
			
			stmt.close(); 
			con.close();
		}
		catch (Exception e) {
			throw new Exception("Unable to find advertisements " + e.getMessage(), e);
		}
	}
	
	
	
	public ArrayList<AdBeans> GetHotelAdvert() throws Exception {
		ArrayList<AdBeans> resAds = new ArrayList<AdBeans>();
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from roomsdataraw");
			
			PreparedStatement stmt2 = con.prepareStatement(
					"select * from hotels where id = ?");
			
			ResultSet rs = stmt.executeQuery();
			int hotelID; 
			
			while (rs.next()) {
				AdBeans currAd = new AdBeans();
				hotelID = rs.getInt("hotel");
				stmt2.setInt(1, hotelID);
				
				ResultSet rs2 = stmt2.executeQuery();
				if(rs2.next()) {
					currAd.sethotelCity(rs2.getString("city"));
					currAd.setHotelName(rs2.getString("name"));
				}
				int i = rs.getInt("type");
				currAd.setRoomType(getRoomType(i));
				currAd.setHotelID(hotelID);
				//getHotelDeets(currAd);
				currAd.setHotelCost(rs.getInt("baseprice"));
				resAds.add(currAd);
			}
			stmt.close();
			stmt2.close();
			con.close();
		}
		catch (Exception e) {
			throw new Exception("Unable to find advertisements " + e.getMessage(), e);
		}
		
		
		return resAds;
	}
	
	public String getRoomType(int typeId) throws DataAccessException {
		String type;
		
		Connection con = null;
		try {
			con = services.createConnection();
			System.out.println("room type id: "+typeId);
			PreparedStatement getType = con.prepareStatement(
					"select * from room_types where id = ?");
			getType.setInt(1, typeId);
			
			ResultSet rs = getType.executeQuery();
			
			rs.next();
			
			type = rs.getString("name");
			
			getType.close();
			rs.close();
			con.close();
			return type;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}


}
