package com.enterprise.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.enterprise.beans.*;
import com.enterprise.common.DBConnectionFactory;
import com.enterprise.common.ServiceLocatorException;

public class ActivationDAO {
	private DBConnectionFactory services;
	
	
	public ActivationDAO() {
		try {
			services = new DBConnectionFactory();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}
	
	public ActivationBean getAcoount(int id, String key) throws Exception {
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from activationWL where person=? ;");
			
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				ActivationBean searchResult = createActivationBean(rs);
				stmt.close();
				return searchResult;
			}
			rs.close();
			stmt.close(); 
			con.close();
			return null;
			
		}
		catch (Exception e) {
			throw new Exception("Unable to update booking " + e.getMessage(), e);
		}
	}
	
	private ActivationBean createActivationBean(ResultSet rs) throws SQLException {
		ActivationBean act = new ActivationBean();
		act.setid(rs.getInt("id"));
		act.setPerson(rs.getInt("person"));
		act.setKey(rs.getString("activationKey"));
		return act;
	}
	
	public void Activate(int id) throws Exception{
		Connection con = null;
		try {
			con = services.createConnection();
			con.setAutoCommit(false);
			PreparedStatement stmtAct = con.prepareStatement(
					"update people set activated=true where id = ?");
			stmtAct.setInt(1, id);
			PreparedStatement stmtDel = con.prepareStatement(
					"delete from activationWL where person = ?");
			stmtDel.setInt(1, id);
			stmtAct.execute();
			stmtDel.execute();
			con.commit();
		}
		catch (Exception e) {
			throw new Exception("Unable to activate account " + e.getMessage(), e);
		}
	}

	
	
}
