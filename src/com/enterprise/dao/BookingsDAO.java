package com.enterprise.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import com.enterprise.beans.*;
import com.enterprise.common.DBConnectionFactory;
import com.enterprise.common.ServiceLocatorException;



public class BookingsDAO {
	
	/**
	 * The service locator to retrieve database connections from
	 */
	private DBConnectionFactory services;

	/** Creates a new instance of ContactDAOImpl */
	public BookingsDAO() {
		try {
			this.services = new DBConnectionFactory();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}
	
	public void updateBooking(BookingsBeans someBooking) throws Exception {
		Connection con = null;
		try {
			System.out.println("** Going to update bookings **");
			System.out.println("Bookee = " + someBooking.getBookee() + " room = " + someBooking.getRoom() + " start: " + someBooking.getStartDate().getTime() + " end: " + someBooking.getEndDate().getTime() + " id: " + someBooking.getID());
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					" UPDATE bookings SET extraBed = ?, start_date = ?, end_date = ?, total_cost = ? WHERE id = ?");
					
				//	"update bookings set Bookee = ?, room = ?, extraBed = ?, start_date = ?, end_date = ?, total_cost = ? where id = ?");
			java.sql.Date startDate = new java.sql.Date(someBooking.getStartDate().getTime());
			java.sql.Date endDate = new java.sql.Date(someBooking.getEndDate().getTime());
		//	stmt.setInt(1, someBooking.getBookee());
		//	stmt.setInt(2, someBooking.getRoom());
			stmt.setBoolean(1, someBooking.getExtraBed());
			stmt.setDate(2, startDate);
			stmt.setDate(3, endDate);
			stmt.setInt(4, someBooking.getCost());
			stmt.setInt(5, someBooking.getID());
			stmt.execute();
			stmt.close(); 
			//rs.close();
			//con.close();
			
		}
		catch (Exception e) {
			throw new Exception("Unable to update booking " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
		
	public ArrayList<BookingsBeans> GetAllUserBookings(int userID) throws DataAccessException {
		ArrayList<BookingsBeans> resList = new ArrayList<BookingsBeans>();
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from roombookings where Bookee = ?");
			stmt.setInt(1, userID);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {	
				
				java.sql.Date rsDate = rs.getDate("start_date");
				//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				java.util.Date todayDate = new java.util.Date();
				System.out.println(dateFormat.format(todayDate)); //2014/08/06 15:59:48
				if ((rsDate.getTime() - (2 * 86400000)) > todayDate.getTime()) {
					// we are more than 2 days from booking
					BookingsBeans booking = createBookingsBean(rs);
					booking = GetHotelDeets(booking);
					resList.add(booking);
				}
				
				//BookingsBeans booking = createBookingsBean(rs);
				//booking = GetHotelDeets(booking);
				//resList.add(booking);
			}
			stmt.close(); 
			rs.close();
			//con.close();
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return resList;
		
	}
	
	public void DeleteBooking(int bookingId) {
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement("delete from bookings where id = ?");
			stmt.setInt(1, bookingId);
			stmt.executeUpdate();
			
			// updating the number of beds available in hotel

			stmt.close(); 
			//con.close();
			//rs.close();
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return;
	}
	
	public BookingsBeans GetHotelDeets(BookingsBeans foo) {
		Connection con = null;
		BookingsBeans updatedDeets = foo;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement("select * from hotels where id = ?");
			stmt.setInt(1, updatedDeets.gethotelID());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {	
				updatedDeets.setHotelName(rs.getString("name"));
				updatedDeets.setCity(rs.getString("city"));
			}
			stmt.close(); 
			rs.close();
			//con.close();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return updatedDeets;
	}
	
	
	public boolean checkAvail(BookingsBeans someBooking) {
		boolean retVal = false;
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement("select * from roomsdataraw r where r.hotel = ? and r.type = ?");
			stmt.setInt(1, someBooking.gethotelID());
			stmt.setInt(2, someBooking.getRoom());
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				// we have a room available
				retVal = true;
			}
			stmt.close(); 
			rs.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		return retVal;
	}
	
	
	public int updatePrice(BookingsBeans someBooking) {
		int retval = -1;
		Connection con = null;
		java.sql.Date surStart = null;
		java.sql.Date surEnd = null;
		java.sql.Date disStart = null;
		java.sql.Date disEnd = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement("select * from roomsdataraw r where r.hotel = ? and r.type = ?");
			stmt.setInt(1, someBooking.gethotelID());
			stmt.setInt(2, someBooking.getRoom());
			System.out.println("hotel id: " + someBooking.gethotelID() + " room id: " + someBooking.getRoom());
			ResultSet rs = stmt.executeQuery();
			int baseprice = 0, extras = 0;
			Double price = 0.0, dailyprice = 0.0, surcharge = 0.0, discount = 0.0;
			if(rs.next()) {
				// we have a room available
				boolean eBed = rs.getBoolean("extrabed");
				
				if (someBooking.getExtraBed()) {
					// we want an extra bed...
					if (eBed == false) {
						// no extra bed for you
						someBooking.setExtraBed(false);
						extras += 0;
					}
					else {
						extras += 35;
					}	
				}
				else {
					// no extra bed
					extras = 0;
				}
				baseprice = rs.getInt("baseprice");
				surcharge = (double) (rs.getInt("surcharge") / 100);
				discount = (double) (rs.getInt("val") / 100);
				java.sql.Date bStartDate = new java.sql.Date(someBooking.getStartDate().getTime());
				java.sql.Date bEndDate = new java.sql.Date(someBooking.getEndDate().getTime());
				
				// check surcharge and discounts
				surStart = rs.getDate("peakstartdate");
				surEnd = rs.getDate("peakenddate");
				disStart = rs.getDate("disstartdate");
				disEnd = rs.getDate("disenddate");
				//System.out.println("*********************************");
				//System.out.println(disStart.toString());
				//System.out.println(disEnd.toString());
				//System.out.println(surStart.toString());
				//System.out.println(surEnd.toString());
				
				
				long currDay = bStartDate.getTime();
				while (currDay <= bEndDate.getTime()) {
					dailyprice = (double) baseprice;	// default when no discounts
					
					if ((surStart != null) && (surEnd != null)) {
						if ((currDay >= surStart.getTime()) && (currDay <= surEnd.getTime())) {
							dailyprice = (baseprice * (1 + surcharge));
						}
					}
					
					if ((disStart != null) && (disEnd != null)) {
						if ((currDay >= disStart.getTime()) && (currDay <= disEnd.getTime())) {
							dailyprice = (baseprice * (1 - discount));
						}
					}
					
					price += dailyprice + extras;
					currDay += 86400000;	// increment by 1 day
				}
				
				someBooking.setCost(price.intValue());
				retval = price.intValue();
				System.out.println("New price is: " + retval);
			}
			stmt.close(); 
			rs.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return retval;
	}
	
	public ArrayList<BookingsBeans> GetUserBookings(int userID, Date checkin, Date checkout) throws DataAccessException {
		ArrayList<BookingsBeans> resList = new ArrayList<BookingsBeans>();
		Connection con = null;
		try {
			con = services.createConnection();
			
			//System.out.println(userID + " " + checkin.toString() + " " + checkout.toString());
			PreparedStatement stmt = con.prepareStatement("select * from roomBookings where bookee = ? and start_date >= ? and end_date <= ?");
			stmt.setInt(1, userID);
			stmt.setDate(2, checkin);
			stmt.setDate(3, checkout);
			ResultSet rs = stmt.executeQuery();
			//System.out.println("xx after query...");
			while (rs.next()) {	
			//	System.out.println("We have results!");
				//PreparedStatement stmt2 = con.prepareStatement("select * from hotels where id = ?");
				//stmt2.setInt(1, rs.getInt(columnIndex))
				Date rsDate = rs.getDate("start_date");
				
				if ((rsDate.getTime() - (2 * 86400000)) > checkin.getTime()) {
					// we are more than 2 days from booking
					BookingsBeans booking = createBookingsBean(rs);
					booking = GetHotelDeets(booking);
					resList.add(booking);
				}
			}
			
			stmt.close(); 
			rs.close();
			con.close();
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		//System.out.println("end result size: " + resList.size());
		return resList;
		
	}


	private BookingsBeans createBookingsBean(ResultSet rs) throws SQLException {
		BookingsBeans booking = new BookingsBeans();
		booking.setBookee(rs.getInt("Bookee"));
		booking.setCost(rs.getInt("total_cost"));
		booking.setEndDate(rs.getDate("end_date"));
		booking.setStartDate(rs.getDate("start_date"));
		booking.setExtraBed(rs.getBoolean("extraBed"));
		booking.setID(rs.getInt("bid"));
		booking.setRoom(rs.getInt("type"));
		booking.sethotelID(rs.getInt("hotel"));
		booking.setCancelStatus(false);
		
		return booking;
	}
	
	public void addBooking(BookingBean booking) throws Exception {
		Connection con = null;
		int n = 0;
		
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
			"insert into bookings(bookee, room, extrabed, start_date, end_date, total_cost) values (?, ?, ?, ?, ?, ?)");
			java.sql.Date startDate = new java.sql.Date(booking.getStartDate().getTime());
			java.sql.Date endDate = new java.sql.Date(booking.getEndDate().getTime());
			
			stmt.setInt(1, booking.getBookee().getId());
			stmt.setInt(2, booking.getRoom().getId());
			if(booking.GetExtrabed().equals("Yes"))
				stmt.setBoolean(3, true);
			else
				stmt.setBoolean(3, false);
			
			stmt.setDate(4, startDate);
			stmt.setDate(5, endDate);
			stmt.setInt(6, booking.getCost());
			
			System.out.println(stmt);
			for(int i=0; i<booking.getRoomnum(); i++){
				n = stmt.executeUpdate();
				System.out.println(n);
			}
			
			
			stmt.close(); 
			con.close();
			if (n != 1)
				throw new DataAccessException("Did not insert one row into database");
		}
		catch (Exception e) {
			throw new Exception("Unable to add booking " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

}
