package com.enterprise.dao;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.enterprise.beans.*;
import com.enterprise.common.DBConnectionFactory;
import com.enterprise.common.ServiceLocatorException;


public class HotelDAO {
	
	/**
	 * The service locator to retrieve database connections from
	 */
	private DBConnectionFactory services;

	/** Creates a new instance of ContactDAOImpl */
	public HotelDAO() {
		try {
			services = new DBConnectionFactory();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean isVacant(int roomId) throws DataAccessException {
		
		Connection con = null;
		boolean vacant = true;
		try {
			con = services.createConnection();
			PreparedStatement getBookings = con.prepareStatement(
					"select * from Bookings where room = ? and checkedIn = true");
			
			ResultSet rs;
			getBookings.setInt(1,roomId);
			rs = getBookings.executeQuery();
			System.out.println("ROOM "+roomId+" empty!!");
			if(rs.next()){
				vacant = false;
				System.out.println("ROOM "+roomId+" not empty");
			}
			getBookings.close(); 
			rs.close();
			con.close();
			return vacant;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		
	}
	
	public ChainBean getChainFromOwner(int pid) throws DataAccessException {
		ChainBean chain = new ChainBean();
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement getChain = con.prepareStatement(
					"select * from Hotel_Chains where owner = ?");
			
			getChain.setInt(1, pid);

			ResultSet rs;
			rs = getChain.executeQuery();
			int cid = 0;
			String name = "";
			
			if(rs.next()){
				cid = rs.getInt("id");
				name = rs.getString("name");
			}
			rs.close();
			getChain.close();
			chain.setId(cid);
			chain.setOwner(pid);
			chain.setName(name);
			
			PreparedStatement getHotels = con.prepareStatement(
					"select * from Hotels where chain = ?");
			getHotels.setInt(1, cid);
			rs = getHotels.executeQuery();
			while(rs.next()){
				System.out.println("Getting next hotel in chain");
				HotelBean hotel = getHotel(rs.getInt("id"));
				chain.addHotel(hotel);
			}
			System.out.println("Done!");
			rs.close();
			getHotels.close();
			con.close();
			
			return chain;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	public HotelBean getHotelFromManager(int pid) throws DataAccessException {
		//HotelBean hotel = new HotelBean();
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement getHotel = con.prepareStatement(
					"select * from Hotels where manager = ?");
			
			getHotel.setInt(1, pid);

			ResultSet rs;
			rs = getHotel.executeQuery();
			int hid = 0;
			if(rs.next()){
				hid = rs.getInt("id");
			}
			rs.close();
			getHotel.close();
			
			HotelBean hotel = getHotel(hid);
			con.close();
			return hotel;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	public HotelBean getHotel(int hid) throws DataAccessException {
		System.out.println("Getting hotel...");
		HotelBean hotel = new HotelBean();
		System.out.println("Created Hotel Bean");
		Connection con = null;
		
		try {
			System.out.println("Attempting to connect");
			con = services.createConnection();
			System.out.println("Connected");
			PreparedStatement getHotel = con.prepareStatement(
					"select * from Hotels where id = ?");
			PreparedStatement getRooms = con.prepareStatement(
					"select * from Rooms where hotel = ?");
			
			
			getHotel.setInt(1, hid);
			getRooms.setInt(1, hid);
			ResultSet rs;
			rs = getHotel.executeQuery();
			System.out.println("HOTEL ID:"+ hid);
			rs.next();
			hotel.setId(hid);
			hotel.setName(rs.getString("name"));
			hotel.setManager(rs.getInt("manager"));
			hotel.setCity(rs.getString("city"));
			//hotel.setOwner(rs.getInt("owner"));
			
			getHotel.close(); 
			rs.close();
			
			rs = getRooms.executeQuery();
			while(rs.next()){
				int rid = rs.getInt("id");
				RoomBean room = getRoomBean(rid);
				if(rs.getBoolean("inMaintenence")){
					hotel.addInMaintenance(room);
				}
				else if(isVacant(rid)){
					hotel.addVacantRoom(room);
				}
				PreparedStatement getBookings = con.prepareStatement(
						"select * from Bookings where room = ?");
				getBookings.setInt(1, rid);
				ResultSet rs2 = getBookings.executeQuery();
				PersonBean p;
				while(rs2.next()){
					int pid = rs2.getInt("bookee");
					p = GetPerson(pid);
					System.out.println("GUEST IS:"+pid);
					System.out.println("Namee IS:"+p.getFirstName());
					System.out.println("Getting date...");
					//rs.getDate("start_date");
					//System.out.println("GOT date!");
					p.setCheckInDate(rs2.getDate("start_date"));
					p.setCheckOutDate(rs2.getDate("end_date"));
					p.setRoomNum(room.getRoomNumAvailable());
					p.setRoomType(room.getRoomType());
					p.setBid(rs2.getInt("id"));
					if(rs2.getBoolean("checkedIn")){
						hotel.addAssignedBooking(p);
					}
					else{
						hotel.addUnassignedBooking(p);
					}
						
					
				}
				rs2.close();
				getBookings.close();
				
				//else
					//hotel.addOccupiedRoom(getRoomBean(rid));
			}
			getRooms.close(); 
			rs.close();
			con.close();
			
			return hotel;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	
	public PersonBean GetPerson(int pid) throws DataAccessException {
		PersonBean person = new PersonBean();
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from PersonData where id = ?");
			stmt.setInt(1, pid);
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
				person = createPersonBean(rs);
			
			stmt.close(); 
			rs.close();
			con.close();
			return person;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	
	public PersonBean GetPersonFromBooking(int bid) throws DataAccessException {
		PersonBean person = new PersonBean();
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt1 = con.prepareStatement(
					"select * from Bookings where id = ?");
			PreparedStatement stmt2 = con.prepareStatement(
					"select * from PersonData where id = ?");
			int pid; Date start; Date end;
			stmt1.setInt(1, bid);
			ResultSet rs = stmt1.executeQuery();
			rs.next();
			pid = rs.getInt("bookee");
			start = rs.getDate("start_date");
			end = rs.getDate("end_date");
			stmt2.setInt(1, pid);
			stmt1.close();
			rs.close();
			rs = stmt2.executeQuery(); 
			while(rs.next())
				person = createPersonBean(rs);
			
			stmt2.close(); 
			rs.close();
			person.setCheckInDate(start);
			person.setCheckOutDate(end);
			con.close();
			return person;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	
	
	
	private PersonBean createPersonBean(ResultSet rs) throws SQLException {
		PersonBean person = new PersonBean();
		person.setPassword(rs.getString("password"));
		person.setRole(rs.getInt("role"));
		person.setUserName(rs.getString("username"));
		person.setNickName(rs.getString("nickname"));
		person.setFirstName(rs.getString("firstName"));
		person.setLastName(rs.getString("lastName"));
		person.setEmail(rs.getString("email"));
		person.setAddress(rs.getString("address"));
		person.setCreditCard(rs.getString("CreditCard"));
		//person.setProfileId(rs.getInt("profileId"));
		person.setId(rs.getInt("id"));
		
		
		return person;
	}
	public RoomBean getRoomBean(int rid) throws DataAccessException {
		RoomBean r = new RoomBean();
		Connection con = null;
		try {
			con = services.createConnection();
			System.out.println("ROOM ID: "+rid);
			PreparedStatement getRooms = con.prepareStatement(
					"select * from rooms where id = ?");
			getRooms.setInt(1, rid);
			System.out.println("Prep statement ready");
			
			ResultSet rs = getRooms.executeQuery();
			//System.out.println(rs.getInt("num"));
			System.out.println("Query Executed");
			rs.next();
			r.setId(rid);
			r.setRoomNumAvailable(rs.getInt("num"));
			int roomType = rs.getInt("type");
			System.out.println("Getting room type: "+roomType);
			r.setRoomType(getRoomType(roomType));
			
			
			rs.close();
			getRooms.close();
			con.close();
			return r;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	public String getRoomType(int typeId) throws DataAccessException {
		String type;
		/*if(typeId == 1){
			type = "single";
		}
		else if(typeId == 2){
			type = "twin";
		}
		else if(typeId == 3){
			type = "queen";
		}
		else if(typeId == 4){
			type = "executive";
		}
		else {
			type = "suite";
		}
		return type;*/
		Connection con = null;
		try {
			con = services.createConnection();
			System.out.println("room type id: "+typeId);
			PreparedStatement getType = con.prepareStatement(
					"select * from room_types where id = ?");
			getType.setInt(1, typeId);
			
			ResultSet rs = getType.executeQuery();
			
			rs.next();
			
			type = rs.getString("name");
			
			getType.close();
			rs.close();
			con.close();
			return type;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	
	public void updateRoomNum(int bid, int rid){
		Connection con = null;
		try {
			con = services.createConnection();
			System.out.println("CONNECTION SUCCESSFUL");
			
			PreparedStatement update = con.prepareStatement(
					"update bookings set room = ?, checkedIn = true where id = ?");
	
			update.setInt(1, rid);
			update.setInt(2, bid);
			
			update.executeUpdate();
			update.close();
			con.close();
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
	}
	
	public void unassignRoom(int bid){
		Connection con = null;
		try {
			con = services.createConnection();
			System.out.println("CONNECTION SUCCESSFUL");
			
			PreparedStatement update = con.prepareStatement(
					"update bookings set checkedIn = false where id = ?");
	
			update.setInt(1, bid);
			
			update.executeUpdate();
			update.close();
			con.close();
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
	}
	
	public boolean createDiscount(int hid, int roomType, int val, Date startDate, Date endDate){
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement check1 = con.prepareStatement(
					"select * from discounts where hotel = ? and roomType = ? and startDate >= ? and startDate <= ?" );
			PreparedStatement check2 = con.prepareStatement(
					"select * from discounts where hotel = ? and roomType = ? and endDate >= ? and endDate <= ?" );
			check1.setInt(1, hid);
			check2.setInt(1,hid);
			System.out.println("Trying to convert date");
			System.out.println(startDate);
			java.sql.Date start = new java.sql.Date(startDate.getTime());
			java.sql.Date end = new java.sql.Date(endDate.getTime());
			System.out.println("Conversion Successful");
			check1.setInt(2, roomType);
			check1.setInt(2, roomType);
			check1.setDate(3, start);
			check1.setDate(4, end);
			check2.setDate(3, start);
			check2.setDate(4, end);
			
			ResultSet rs = check1.executeQuery();
			if(rs.next()){
				System.out.println("START DATE CONFLICT, NOT ADDING");
				//return false;
			}
			rs.close();
			check1.close();
			
			rs = check2.executeQuery();
			if(rs.next()){
				System.out.println("START DATE CONFLICT, NOT ADDING");
				//return false;
			}
			rs.close();
			check2.close();
			
			PreparedStatement insert = con.prepareStatement(
					"insert into discounts (hotel, roomtype, val, startdate, enddate) values (?,?,?,?,?)");//hotel = ?, roomtype = ?, val = ?, startdate = ?, enddate = ?");
	
			insert.setInt(1, hid);
			insert.setInt(2, roomType);
			insert.setInt(3, val);
			insert.setDate(4, start);
			insert.setDate(5, end);
			insert.executeUpdate();
			insert.close();
			con.close();
			return true;
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
	
	}
	
	public boolean createPeak(int hid, int surcharge, Date startDate, Date endDate){
		Connection con = null;
		
		try {
			con = services.createConnection();
			
			PreparedStatement check1 = con.prepareStatement(
					"select * from peaks where hotel = ? and startDate >= ? and startDate <= ?" );
			PreparedStatement check2 = con.prepareStatement(
					"select * from peaks where hotel = ? and endDate >= ? and endDate <= ?" );
			check1.setInt(1, hid);
			check2.setInt(1,hid);
			java.sql.Date start = new java.sql.Date(startDate.getTime());
			java.sql.Date end = new java.sql.Date(endDate.getTime());
			check1.setDate(2, start);
			check1.setDate(3, end);
			check2.setDate(2, start);
			check2.setDate(3, end);
			
			ResultSet rs = check1.executeQuery();
			if(rs.next()){
				System.out.println("START CONFLICT");
				//return false;
			}
			rs.close();
			check1.close();
			
			rs = check2.executeQuery();
			if(rs.next()){
				System.out.println("END CONFLICT");
				//	return false;
			}
			rs.close();
			check2.close();
			
			PreparedStatement insert = con.prepareStatement(
					"insert into peaks (hotel, surcharge, startdate, enddate) values (?,?,?,?)");//"insert into peaks hotel = ?, surcharge = ?, startdate = ?, enddate = ?");
	
			insert.setInt(1, hid);
			insert.setInt(2, surcharge);
			insert.setDate(3,start);
			insert.setDate(4, end);
			insert.executeUpdate();
			insert.close();
			con.close();
			return true;
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
	
	}
	
	public void deleteDiscount(int id){
		Connection con = null;
		try {
			con = services.createConnection();
			System.out.println("******DELETING******");
			PreparedStatement delete = con.prepareStatement(
					"delete from discounts where id = ?");
	
			delete.setInt(1, id);
			
			delete.executeUpdate();
			delete.close();
			con.close();
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
		
	}
	
	public void deletePeak(int id){
		Connection con = null;
		try {
			con = services.createConnection();
			
			PreparedStatement delete = con.prepareStatement(
					"delete from peaks where id = ?");
	
			delete.setInt(1, id);
			
			delete.executeUpdate();
			delete.close();
			con.close();
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
		
	}
	
	public void setMaintenance(int id){
		Connection con = null;
		try {
			con = services.createConnection();
			
			PreparedStatement update = con.prepareStatement(
					"update rooms set inMaintenence = true where id = ?");
	
			update.setInt(1, id);
			
			update.executeUpdate();
			update.close();
			con.close();
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
		
	}
	
	public void removeMaintenance(int id){
		Connection con = null;
		try {
			con = services.createConnection();
			
			PreparedStatement update = con.prepareStatement(
					"update rooms set inMaintenence = false where id = ?");
	
			update.setInt(1, id);
			
			update.executeUpdate();
			update.close();
			con.close();
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
		
	}
	
	public ArrayList<DiscountBean> getDiscounts(int hid){
		Connection con = null;
		ArrayList<DiscountBean> discounts = new ArrayList<DiscountBean>();
		try {
			con = services.createConnection();
			PreparedStatement getName = con.prepareStatement(
					"select * from hotels where id = ?");
			PreparedStatement stmt = con.prepareStatement(
					"select * from discounts where hotel = ?");
	
			stmt.setInt(1, hid);
			getName.setInt(1, hid);
			ResultSet rs = getName.executeQuery();
			rs.next();
			String name = rs.getString("name");
			rs.close();
			getName.close();
			
			rs = stmt.executeQuery();
			while(rs.next()){
				DiscountBean d = new DiscountBean();
				d.setId(rs.getInt("id"));
				d.setEndDate(rs.getDate("enddate"));
				d.setStartDate(rs.getDate("startdate"));
				d.setHid(hid);
				d.setHotel(name);
				d.setRoomType(getRoomType(rs.getInt("roomtype")));
				d.setValue(rs.getInt("val"));
				discounts.add(d);
			}
			stmt.close();
			rs.close();
			con.close();
			
			return discounts;
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
		
	}
	
	public ArrayList<PeakBean> getPeaks(int hid){
		Connection con = null;
		ArrayList<PeakBean> peaks = new ArrayList<PeakBean>();
		try {
			con = services.createConnection();
			PreparedStatement getName = con.prepareStatement(
					"select * from hotels where id = ?");
			PreparedStatement stmt = con.prepareStatement(
					"select * from peaks where hotel = ?");
	
			stmt.setInt(1, hid);
			getName.setInt(1, hid);
			ResultSet rs = getName.executeQuery();
			rs.next();
			String name = rs.getString("name");
			rs.close();
			getName.close();
			
			rs = stmt.executeQuery();
			while(rs.next()){
				PeakBean p = new PeakBean();
				p.setId(rs.getInt("id"));
				p.setEndDate(rs.getDate("enddate"));
				p.setStartDate(rs.getDate("startdate"));
				p.setHid(hid);
				p.setHotel(name);
				p.setSurcharge(rs.getInt("surcharge"));
				
				peaks.add(p);
			}
			stmt.close();
			rs.close();
			con.close();
			
			return peaks;
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
		
	}

}
