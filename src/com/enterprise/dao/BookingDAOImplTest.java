/*
 * Created on 10/08/2003
 *
 */
package com.enterprise.dao;


import javax.naming.*;

import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import com.enterprise.beans.*;
import com.enterprise.common.DBConnectionFactory;

/**
 */
public class BookingDAOImplTest extends TestCase {

	SearchDAOImpl searchDAOImpl;
	
	/**
	 * Constructor for ContactDAOImplTest.
	 * @param arg0
	 */
	public BookingDAOImplTest(String arg0) {
		super(arg0);
	}

    public void tearDown() throws Exception {
         // To test JNDI outside Tomcat, we need to set these
         // values manually ... (just for testing purposes)
       System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
           "org.apache.naming.java.javaURLContextFactory");
       System.setProperty(Context.URL_PKG_PREFIXES, 
           "org.apache.naming");            

       // Create InitialContext with java:/comp/env/jdbc
       InitialContext ic = new InitialContext();

       ic.unbind("java:/comp/env/jdbc/HSQLDB");
       
       ic.destroySubcontext("java:/comp/env/jdbc");
       ic.destroySubcontext("java:/comp/env");
       ic.destroySubcontext("java:/comp");
       ic.destroySubcontext("java:");
        
    }
    public void setUp() throws Exception {
         // To test JNDI outside Tomcat, we need to set these
         // values manually ... (just for testing purposes)
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
            "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES, 
            "org.apache.naming");            

        // Create InitialContext with java:/comp/env/jdbc
        InitialContext ic = new InitialContext();

        ic.createSubcontext("java:");
        ic.createSubcontext("java:/comp");
        ic.createSubcontext("java:/comp/env");
        ic.createSubcontext("java:/comp/env/jdbc");
       
        // Construct BasicDataSource reference
        Reference ref = new Reference("javax.sql.DataSource", "org.apache.commons.dbcp.BasicDataSourceFactory", null);
        ref.add(new StringRefAddr("driverClassName", "org.hsqldb.jdbcDriver"));
        
        //TODO: change the following to your own setting
        ref.add(new StringRefAddr("url", "jdbc:hsqldb:F:/UNSW/term1/comp9321/HSQLDB;ifexists=true"));
        ref.add(new StringRefAddr("username", "sa"));
        ref.add(new StringRefAddr("password", ""));
        ic.bind("java:/comp/env/jdbc/HSQLDB", ref);
    
        searchDAOImpl = new SearchDAOImpl(getDBConnectionFactory());
    }

	public void testAddEntry() throws Exception {
		BookingsDAO bookingsDAO = new BookingsDAO();
		PersonBean person = new PersonBean();
		person.setId(10);
		RoomBean room = new RoomBean();
		room.setId(0);
		
		
		
		
		BookingBean booking = new BookingBean();
		booking.setRoomnum(10);
		booking.setBookee(person);
		booking.setCost(120);
	//	booking.setEndDate(endDate);
	//	booking.setStartDate(startDate);
		booking.setRoom(room);
		
		booking.setExtrabed("Yes");
		
		bookingsDAO.addBooking(booking);
		
	}
	
	public DBConnectionFactory getDBConnectionFactory() throws Exception {
		DBConnectionFactory factory = new DBConnectionFactory();
		return factory;
	}
}
