package com.enterprise.dao;


import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.SecureRandom;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.enterprise.beans.*;
import com.enterprise.common.DBConnectionFactory;
import com.enterprise.common.SendEmail;
import com.enterprise.common.ServiceLocatorException;


public class PeopleDAO {
	
	/**
	 * The service locator to retrieve database connections from
	 */
	private DBConnectionFactory services;

	/** Creates a new instance of ContactDAOImpl */
	public PeopleDAO() {
		try {
			services = new DBConnectionFactory();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}
	public void updatePerson(PersonBean person) throws DataAccessException {
		Connection con = null;
		try {
			System.out.println("TRYING TO UPDATE: ");
			//System.out.println("NEW DETAILS: ");
			//System.out.println(person.getId()+" "+person.getUserName());
			con = services.createConnection();
			System.out.println("CONNECTION SUCCESSFUL");
			/*
			PreparedStatement updatePerson = con.prepareStatement(
					"update people set (firstName, lastName, email, password) values (?, ?, ?, ?) where id = ?");
			PreparedStatement updateProfile = con.prepareStatement(
					"update profile set (creditCardNo,nickname,address,phoneNumber) values (?, ?, ?, ?) where id = ?"
					);*/
			PreparedStatement updatePerson = con.prepareStatement(
					"update people set firstName=?, lastName=?, email=?, password=? where id = ?");
			//System.out.println("Prep stmt 1");
			PreparedStatement updateProfile = con.prepareStatement(
					"update profiles set creditCardNo=?,nickname=?,address=?,phoneNumber=? where person = ?"
					);	
			//System.out.println("Prep stmt 2");
			//PreparedStatement updatePerson = con.prepareStatement(
			//		"update people set (id,firstName,lastName,email,role,username,password,profile) values (?, ?, ?, ?, ?, ?, ?, ?) where id = ?");
			//java.sql.Date startDate = new java.sql.Date(someBooking.getStartDate().getTime());
			//java.sql.Date endDate = new java.sql.Date(someBooking.getEndDate().getTime());
			
			//updateProfile.setInt(1,person.getId());
			updateProfile.setString(1,person.getCreditCard());
			updateProfile.setString(2,person.getNickName());
			updateProfile.setString(3, person.getAddress());
			updateProfile.setString(4, person.getPhone());
			updateProfile.setInt(5,person.getId());
			
			System.out.println("Prep stmt 1");
			//updatePerson.setInt(1, person.getId());
			updatePerson.setString(1, person.getFirstName());
			updatePerson.setString(2,person.getLastName());
			updatePerson.setString(3, person.getEmail());
			//updatePerson.setInt(4, person.getRole());
			//updatePerson.setString(5, person.getUserName());
			updatePerson.setString(4, person.getPassword());
			//updatePerson.setInt(8,person.getProfileId());
			updatePerson.setInt(5, person.getId());
			System.out.println("NEW DETAILS: ");
			System.out.println(person.getId()+" "+person.getUserName());
			updateProfile.executeUpdate();
			updatePerson.executeUpdate();
			System.out.println("Query has been executed, data changed");
			updateProfile.close();
			updatePerson.close();
			con.close();
			
		}
		catch (Exception e) {
			System.out.println("couldn't update");
			throw new DataAccessException("Unable to update person1 " + e.getMessage(), e);
		}
	}
	
	public void createPerson(PersonBean person) throws DataAccessException {
		Connection con = null;
		try {
			con = services.createConnection();
			
			PreparedStatement addPerson = con.prepareStatement(
					"insert into people(firstName, lastname, email, role, username, password)  values (?, ?, ?, 3, ?, ?)");
			
			//java.sql.Date startDate = new java.sql.Date(someBooking.getStartDate().getTime());
			//java.sql.Date endDate = new java.sql.Date(someBooking.getEndDate().getTime());
			
	/*		addProfile.setInt(1,person.getId());
			addProfile.setString(2,person.getCreditCard());
			addProfile.setString(3,person.getNickName());
			addProfile.setString(4, person.getAddress());
			addProfile.setString(5, person.getPhone());
			addProfile.executeUpdate();
			addProfile.close(); */
			
			addPerson.setString(1, person.getFirstName());
			addPerson.setString(2, person.getLastName());
			addPerson.setString(3, person.getEmail());
			addPerson.setString(4, person.getUserName());
			addPerson.setString(5, person.getPassword());
			
			addPerson.execute();
			
			addPerson.close();
			PreparedStatement ret = con.prepareStatement(
					"select * from people where username = ? and password = ?");
			ret.setString(1, person.getUserName());
			ret.setString(2, person.getPassword());
			ResultSet rs = ret.executeQuery();
			if (rs.next()) {
				person.setId(rs.getInt("id"));
			}
		    for (int i=0; i<10; i++){
		    	System.out.println(person.getId());
		    }
			//generate random string
			SecureRandom random = new SecureRandom();
			String randstring = new BigInteger(130, random).toString(29);
			  for (int i=0; i<10; i++){
			    	System.out.println(randstring);
			    }
			//put it in the activation database
			PreparedStatement addActivationLink = con.prepareStatement(
					"insert into activationWL(person, activationKey) values (?, ?)");
			addActivationLink.setInt(1, person.getId());
			addActivationLink.setString(2, randstring);
			addActivationLink.executeUpdate();
			addActivationLink.close();
			System.out.println("UPDATE SUCCESSFULL");
			
			SendEmail message = new SendEmail();
			message.sendEmailMessage(person.getId(), randstring, person.getEmail());

			PreparedStatement addProfile = con.prepareStatement(
					"insert into profiles(creditCardNo, nickname, address, phoneNumber, person) values (?, ?, ?, ?, ?)");
			addProfile.setString(1, null);
			addProfile.setString(2,null);
			addProfile.setString(3, null);
			addProfile.setString(4, null);
			addProfile.setInt(5, person.getId());
			addProfile.executeUpdate();
			addProfile.close(); 
			con.close();
		}
		catch (Exception e) {
			System.out.println("couldn't add");
			throw new DataAccessException("Unable to add person " + e.getMessage(), e);
		}
	}
	
	public PersonBean GetPerson(int userID) throws DataAccessException {
		PersonBean person = new PersonBean();
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from PersonData where ID = ?");
			stmt.setInt(1, userID);
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
				person = createPersonBean(rs);
			stmt.close(); 
			rs.close();
			con.close();
			return person;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	public PersonBean GetPerson(String username) throws DataAccessException {
		PersonBean person = new PersonBean();
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from PersonData where username = ?");
			stmt.setString(1, username);
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
				person = createPersonBean(rs);
			
			stmt.close(); 
			rs.close();
			con.close();
			return person;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	public boolean usernameExists(String username) throws DataAccessException {
		
		boolean personExists = false;
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from PersonData where username = ?");
			stmt.setString(1, username);
			ResultSet rs = stmt.executeQuery();
			if(rs.next())
				personExists = true;
			
			
			stmt.close(); 
			rs.close();
			con.close();
			return personExists;
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}
	public String GetPassword(String userName) throws DataAccessException {
		
		Connection con = null;

		try {
			con = services.createConnection();
			System.out.println("USERNAME: "+userName);
			PreparedStatement stmt = con.prepareStatement(
					"select * from people where username = ?");
			stmt.setString(1, userName);
			ResultSet rs = stmt.executeQuery();
			System.out.println("QUERY EXECUTED SUCCESSFULLY");
			//PersonBean person = new PersonBean();
			String password = "";
			while(rs.next()){
				System.out.println("Here?");
				password = rs.getString("password");
			}
			System.out.println("QUERY EXECUTED SUCCESSFULLY 2");
			stmt.close(); 
			rs.close();
			System.out.println("password is: "+password);
			con.close();
			return password;
			
		} catch (ServiceLocatorException e) {
			System.out.println("Error");
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}

	private PersonBean createPersonBean(ResultSet rs) throws SQLException {
		PersonBean person = new PersonBean();
		person.setPassword(rs.getString("password"));
		person.setRole(rs.getInt("role"));
		person.setUserName(rs.getString("username"));
		person.setNickName(rs.getString("nickname"));
		person.setFirstName(rs.getString("firstName"));
		person.setLastName(rs.getString("lastName"));
		person.setEmail(rs.getString("email"));
		person.setAddress(rs.getString("address"));
		person.setCreditCard(rs.getString("CreditCard"));
		//person.setProfileId(rs.getInt("profileId"));
		person.setId(rs.getInt("id"));
		
		
		return person;
	}
	

}
