package com.enterprise.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.enterprise.beans.RoomBean;
import com.enterprise.common.DBConnectionFactory;
import com.enterprise.common.ServiceLocatorException;
import com.enterprise.dao.DataAccessException;

public class SearchDAOImpl {
private DBConnectionFactory services;
	RoomBean room;
	public SearchDAOImpl() {
		try {
			services = new DBConnectionFactory();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}

	public SearchDAOImpl(DBConnectionFactory services) {
		this.services = services;
	}

	public List<RoomBean> searchHotel(java.util.Date check_in_date, java.util.Date check_out_date,
				String city, int room_nums, int max_price) 
			throws DataAccessException {
		Connection con = null;
		List<RoomBean> searchResultList = new ArrayList<RoomBean>();
		try {

			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from roomsDataRaw "
					+ "where city=? and num>=? "
					+ "order by id ");
			
			
			stmt.setString(1, city);
			stmt.setInt(2, room_nums);
//			stmt.setInt(3, max_price);

			
			ResultSet rs = stmt.executeQuery();
			
			int cur_roomid = -1;
			while (rs.next()) {
				room = createRoomBean(rs, con, check_in_date, check_out_date);
				if(room.getId() == cur_roomid){
					continue;
				}
				cur_roomid = room.getId();
				
				// calculate available room number
				room.setRoomNumAvailable(room.getRoomNumAvailable() - getBookedRooms(con, check_in_date, check_out_date, room));
				
				// check discount
				handleDiscount(con, check_in_date, check_out_date, cur_roomid);
				
				// check period
				handlePeriod(con, check_in_date, check_out_date, cur_roomid);
				
				if(room.getRoomNumAvailable() >= room_nums && 
						room.getPrice() <= max_price)
					searchResultList.add(room); 
			}

			rs.close(); 
			stmt.close(); 
			con.close();
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		return searchResultList;
	}

	private RoomBean createRoomBean(ResultSet rs, Connection con, Date check_in_date, Date check_out_date) throws SQLException {
		RoomBean room = new RoomBean();
		int room_actual_price = 0;
		
		room_actual_price = rs.getInt("baseprice");
		
		room.setId(rs.getInt("id"));
		room.setHotel(rs.getString("hotelname"));
		room.setPrice(rs.getInt("baseprice"));
		room.setRoomNumAvailable(rs.getInt("num"));
		room.setRoomType(rs.getString("name"));
		room.setPeriod("off-Peak");
		
		
		/*if(rs.getDate("disStartDate") != null && rs.getDate("disEndDate") != null){
			java.util.Date disStartDate = new java.util.Date(rs.getDate("disStartDate").getTime());
			java.util.Date disEndDate = new java.util.Date(rs.getDate("disEndDate").getTime());
			float discount = rs.getInt("val");
			
			if(disStartDate.compareTo(check_in_date) <= 0 && 
					disEndDate.compareTo(check_out_date) >=0){
				room_actual_price = (int) (room.getPrice()*discount/100);
			}
		}
		
		if(rs.getDate("peakStartdate") != null && rs.getDate("peakEndDate") != null){
			java.util.Date peakStartdate = new java.util.Date(rs.getDate("peakStartdate").getTime());
			java.util.Date peakEndDate = new java.util.Date(rs.getDate("peakEndDate").getTime());
			float surcharge = rs.getInt("surcharge");
			if(peakStartdate.compareTo(check_in_date) <= 0 && 
					peakEndDate.compareTo(check_out_date) >=0){
				room_actual_price = (int) (room.getPrice()*(surcharge/100+1));
				room.setPeriod("Peak");
			}
		}*/
		
		return room;
	}
	
	public java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
	    return new java.sql.Date(date.getTime());
	}
	
	public int getBookedRooms(Connection con, Date check_in_date, Date check_out_date, RoomBean room) throws SQLException {
		int available_rooms = 0;
		
		PreparedStatement stmt_a = con.prepareStatement(
				"select count(1) from " +
				"(select * from roomBookings " +
				"where (((start_date <= ? and end_date > ?) or " +
				"(start_date <= ? and start_date>=?)) and " +
				"start_date is not NULL) " +
				"and id = ? and inMaintenence = false)");
		
		stmt_a.setDate(1, convertJavaDateToSqlDate(check_in_date));
		stmt_a.setDate(2, convertJavaDateToSqlDate(check_in_date));
		stmt_a.setDate(3, convertJavaDateToSqlDate(check_out_date));
		stmt_a.setDate(4, convertJavaDateToSqlDate(check_in_date));

		stmt_a.setInt(5, room.getId());
		
		ResultSet rs_av_room = stmt_a.executeQuery();
		if(rs_av_room.next()){
			available_rooms =  rs_av_room.getInt(1);
		}
		
		rs_av_room.close();
		stmt_a.close(); 
		
		return available_rooms;
	}
	
	
	private void handleDiscount(Connection con, Date check_in_date, Date check_out_date, int cur_roomid) throws SQLException {
		int room_actual_price;
		
		PreparedStatement stmt;
		
		stmt = con.prepareStatement(
				"select a.val, a.startdate, a.enddate from discounts a "
				+ "join rooms b on a.hotel = b.hotel "
				+ "where b.id = ?");
	
		stmt.setInt(1, cur_roomid);
		
//		System.out.println(stmt);
		
		ResultSet rs = stmt.executeQuery();
		while(rs.next()){
			java.util.Date disStartDate = new java.util.Date(rs.getDate("startdate").getTime());
			java.util.Date disEndDate = new java.util.Date(rs.getDate("enddate").getTime());
			float discount = rs.getInt("val");


			if(disStartDate.compareTo(check_in_date) <= 0 && 
					disEndDate.compareTo(check_out_date) >=0){
				room_actual_price = (int) ((float)room.getPrice() * (1 - discount/100.0));
				System.out.println(room_actual_price);
				room.setPrice(room_actual_price);
			}
		} 
		
		rs.close();
		stmt.close();
	}
	
	
	private void handlePeriod(Connection con, Date check_in_date, Date check_out_date, int cur_roomid) throws SQLException {
		int room_actual_price;
		
		PreparedStatement stmt = con.prepareStatement(
				"select a.surcharge, a.startdate, a.enddate from peaks a join rooms b " +
				"on a.hotel = b.hotel " +
				"where b.id = ?");
		
		stmt.setInt(1, cur_roomid); 
		
	//	System.out.println(stmt);
	
		ResultSet rs = stmt.executeQuery();
		while(rs.next()){
			java.util.Date peakStartdate = new java.util.Date(rs.getDate("startdate").getTime());
			java.util.Date peakEndDate = new java.util.Date(rs.getDate("enddate").getTime());
			float surcharge = rs.getInt("surcharge");
			if(peakStartdate.compareTo(check_in_date) <= 0 && 
					peakEndDate.compareTo(check_out_date) >=0){
				room_actual_price = (int) ((float)room.getPrice()*(surcharge/100.0+1.0));
				room.setPrice(room_actual_price);
				room.setPeriod("Peak");
			}
		}
	
		rs.close();
		stmt.close();
		
	}
}
