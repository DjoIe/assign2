package com.enterprise.common;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmail
{
    private class SMTPAuthenticator extends Authenticator
    {
        public PasswordAuthentication getPasswordAuthentication()
        {
            return new PasswordAuthentication("bookingsdotyeahdotcom@gmail.com", "Assignment2");
        }
    }




   public void sendEmailMessage(int id, String rand, String email) throws MessagingException {

          // Get system properties
        Properties props = System.getProperties();
        props = new Properties();
            props.put("mail.smtp.user", "bookingsdotyeahdotcom@gmail.com");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.starttls.enable","true");
            props.put("mail.smtp.debug", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.socketFactory.port", "587");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");


            SMTPAuthenticator auth = new SMTPAuthenticator();
        Session session = Session.getInstance(props, auth);
            session.setDebug(false);

        MimeMessage msg = new MimeMessage(session);
            msg.setText("Follow this link to activate your account "
			  		+ "http://localhost:8080/BookingsDotYeah/Activate?id="+id
			  		+ "&link="+rand);
            msg.setSubject("Account Activation");
            msg.setFrom(new InternetAddress("bookingdotyeahdotcom@gmail.com"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

            Transport transport = session.getTransport("smtps");
            transport.connect("smtp.gmail.com", 465, "username", "password");
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();  

   }

}