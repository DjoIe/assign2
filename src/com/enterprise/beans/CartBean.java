package com.enterprise.beans;

import java.util.ArrayList;
import java.util.List;

public class CartBean {
	private PersonBean userBean;
	private List<BookingBean> bookings;
	private int totalCost;
	
	public CartBean(){
		bookings = new ArrayList<BookingBean>();
	}
	
	public PersonBean getUserBean() {
		return userBean;
	}
	public void setUserBean(PersonBean user) {
		this.userBean = user;
	}
	public List<BookingBean> getBookings() {
		return bookings;
	}
	public void setBookings(List<BookingBean> bookings) {
		this.bookings = bookings;
	}
	public int getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}
	

}
