package com.enterprise.beans;

public class AdBeans {
	
	private String hotelName;
	private String hotelCity;
	private int hotelCost;
	private int hotelID;
	private String roomType;
	
	public AdBeans() {
		hotelName = "";
		hotelCity = "";
		roomType = "";
		hotelCost = -1;
		hotelID = -1;
	}
	public int getHotelID() {
		return hotelID;
	}
	
	public void setHotelID(int foo) {
		hotelID = foo;
	}
	
	public String getRoomType() {
		return roomType;
	}
	
	public void setRoomType(String foo) {
		roomType = foo;
	}
	
	public String getHotelName() {
		return hotelName;
	}
	
	public void setHotelName(String foo) {
		hotelName = foo;
	}
	
	public String gethotelCity() {
		return hotelCity;
	}
	
	public void sethotelCity(String foo) {
		hotelCity = foo;
	}
	
	public int getHotelCost () {
		return hotelCost; 
	}
	
	public void setHotelCost(int foo) {
		hotelCost = foo;
	}
	
}
