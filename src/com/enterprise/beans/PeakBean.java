package com.enterprise.beans;

import java.io.Serializable;
import java.util.Date;

public class PeakBean implements Serializable  {
	Date startDate, endDate;
	int id, surcharge, hid;
	String hotel;
	
	public Date getStartDate(){
		return startDate;
	}
	public Date getEndDate(){
		return endDate;
	}
	public int getId(){
		return id;
	}
	public int getSurcharge(){
		return surcharge;
	}
	public int getHid(){
		return hid;
	}
	public String getHotel(){
		return hotel;
	}
	
	
	public void setStartDate(Date d){
		startDate = d;
	}
	public void setEndDate(Date d){
		endDate = d;
	}
	public void setId(int id){
		this.id = id;
	}
	public void setHid(int id){
		hid = id;
	}
	public void setSurcharge(int v){
		surcharge = v;
	}
	public void setHotel(String s){
		hotel = s;
	}
	
	public PeakBean(){
		
	}
}
