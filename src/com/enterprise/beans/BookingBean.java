package com.enterprise.beans;


public class BookingBean {
	private PersonBean bookee;
	private RoomBean room;
	private int roomnum;
	private String extrabed;
	private java.util.Date startDate;
	private java.util.Date endDate;
	private int cost;
	
	public PersonBean getBookee() {
		return bookee;
	}
	public void setBookee(PersonBean person) {
		this.bookee = person;
	}
	public RoomBean getRoom() {
		return room;
	}
	public void setRoom(RoomBean room) {
		this.room = room;
	}
	public int getRoomnum() {
		return roomnum;
	}
	public void setRoomnum(int roomnum) {
		this.roomnum = roomnum;
	}
	public String GetExtrabed() {
		return extrabed;
	}
	public void setExtrabed(String extrabed) {
		this.extrabed = extrabed;
	}
	public java.util.Date getStartDate() {
		return startDate;
	}
	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}
	public java.util.Date getEndDate() {
		return endDate;
	}
	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	
}
