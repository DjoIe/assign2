package com.enterprise.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class HotelBean implements Serializable  {
	private int id;
	private String name;
	private int owner;
	private int manager;
	private String city;
	ArrayList<RoomBean> vacantRooms;
	ArrayList<RoomBean> inMaintenance;
	//ArrayList<RoomBean> occupiedRooms;
	ArrayList<PersonBean> assignedBookings;
	ArrayList<PersonBean> unassignedBookings;
	int numVacantRooms;
	int numOccupiedRooms;
	
	public String getCity(){
		return city;
	}
	public void setCity(String city){
		this.city = city;
	}
	public ArrayList<RoomBean> getVacantRooms(){
		return vacantRooms;
	}
	public ArrayList<RoomBean> getInMaintenance(){
		return inMaintenance;
	}
	public ArrayList<PersonBean> getAssignedBookings(){
		return assignedBookings;
	}
	public ArrayList<PersonBean> getUnassignedBookings(){
		return unassignedBookings;
	}
	/*public ArrayList<RoomBean> getOccupiedRooms(){
		return occupiedRooms;
	}
	public void addOccupiedRoom(RoomBean room){
		occupiedRooms.add(room);
	}*/
	public void addVacantRoom(RoomBean room){
		vacantRooms.add(room);
	}
	public void addInMaintenance(RoomBean room){
		inMaintenance.add(room);
	}
	public void addAssignedBooking(PersonBean p){
		assignedBookings.add(p);
	}
	public void addUnassignedBooking(PersonBean p){
		unassignedBookings.add(p);
	}
	public int getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public int getOwner(){
		return owner;
	}
	public int getManager(){
		return manager;
	}
	public int getNumVacantRooms(){
		return vacantRooms.size();
	}
	public int getNumOccupiedRooms(){
		return assignedBookings.size();
	}
	
	public void setId(int id){
		this.id = id;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setOwner(int owner){
		this.owner = owner;
	}
	public void setManager(int manager){
		this.manager = manager;
	}
	
	public HotelBean(){
		vacantRooms = new ArrayList<RoomBean>();
		inMaintenance = new ArrayList<RoomBean>();
		//ArrayList<RoomBean> occupiedRooms;
		assignedBookings = new ArrayList<PersonBean>();
		unassignedBookings = new ArrayList<PersonBean>();
	}
	
}
