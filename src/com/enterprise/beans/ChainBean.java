package com.enterprise.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class ChainBean implements Serializable  {
	private int id;
	private String name;
	private int owner;
	//private String manager;
	ArrayList<HotelBean> hotels;
	
	public ChainBean(){
		hotels = new ArrayList<HotelBean>();
	}

	public void addHotel(HotelBean hotel) {
		hotels.add(hotel);
		
	}
	public void setName(String name){
		this.name = name;
	}
	public void setOwner(int o){
		owner = o;
	}
	public void setId(int cid) {
		id = cid;
		
	}
	public int getId(){
		return id;
	}
	public int getOwner(){
		return owner;
	}
	public String getName(){
		return name;
	}
	public ArrayList<HotelBean> getHotels(){
		return hotels;
	}
	
}
