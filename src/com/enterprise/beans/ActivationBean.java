package com.enterprise.beans;

public class ActivationBean {
	private int id;
	private int person;
	private String key;
	

	public int getid() {
		return id;
	}
	public void setid(int id) {
		this.id = id;
	}
	public int getPerson() {
		return person;
	}
	public void setPerson(int person) {
		this.person = person;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
}
