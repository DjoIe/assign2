package com.enterprise.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class BookingsBeans implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;		// booking id
	private int Bookee;	// id of person doing the booking same as people(id)
	private int room; 	// type of room, same as room(id)
	private boolean extraBed; 	// whether extra bed is needed
	private java.util.Date startDate;	// start date need to convert from sql.date
	private java.util.Date endDate;
	private int totalCost; 		// cost of this booking
	private boolean cancelStatus; 
	private String hotelName;
	private int hotelID;
	private DateFormat dateFormat;
	private String city;
	
	public BookingsBeans() {
		id = -1;
		Bookee = -1;
		room = -1;
		extraBed = false;
		startDate = new java.util.Date(0);
		endDate = new java.util.Date(0);
		totalCost = -1;
		cancelStatus = false;
		hotelName = "";
		hotelID = -1;
		city = "";
		dateFormat = new SimpleDateFormat("dd/MMM/yy");
		
	}
	
	public boolean equals(BookingsBeans someOtherBooking) {
		boolean retval = false;
		if(this.id == someOtherBooking.id) {
			retval = true;
		}
		return retval;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String foo) {
		city = foo;
	}
	public int gethotelID() {
		return hotelID;
	}
	
	public void sethotelID(int foo) {
		hotelID = foo;
	}
	
	public int getID() {
		return id;
	}
	
	public int getBookee() {
		return Bookee;
	}
	
	public int getRoom() {
		return room;
	}
	
	public boolean getExtraBed() {
		return extraBed;
	}
	
	public java.util.Date getStartDate() {
		return startDate;
	}
	
	public java.util.Date getEndDate() {
		return endDate;
	}
	
	public String getStartDateString() {
		String foo = this.dateFormat.format(startDate); 
		return foo;
	}
	
	public String getEndDateString() {
		String foo = this.dateFormat.format(endDate);
		return foo;
	}
	
	public int getCost() {
		return totalCost;
	}
	
	public void setID(int foo) {
		id = foo;
	}
	
	public void setBookee(int foo) {
		Bookee = foo;
	}
	
	public void setRoom(int foo) {
		room = foo;
	}
	
	public void setExtraBed(boolean foo) {
		extraBed = foo;
	}
	
	public void setStartDate(java.util.Date foo) {
		startDate = foo;
	}
	
	public void setStartDate(java.sql.Date foo) {
		startDate = new java.util.Date(foo.getTime());
	}
	
	public void setEndDate(java.util.Date foo) {
		endDate = foo;
	}
	
	public void setEndDate(java.sql.Date foo) {
		endDate = new java.util.Date(foo.getTime());
	}
	public void setCost(int foo) {
		totalCost = foo;
	}

	public void setCancelStatus(boolean b) {
		// TODO Auto-generated method stub
		cancelStatus = b;
	}
	
	public boolean getCancelStatus() {
		return cancelStatus;
	}

	public void setHotelName(String string) {
		// TODO Auto-generated method stub
		hotelName = string;
	}
	
	public String getHotelName(){
		return hotelName;
	}


}
