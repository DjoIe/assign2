package com.enterprise.beans;

public class RoomBean {
	private int id;
	private String hotel;
	private String roomType;
	private int roomNumAvailable;
	private int price;
	private String period;
	

	public String getHotel() {
		return hotel;
	}
	public void setHotel(String hotel) {
		this.hotel = hotel;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public int getRoomNumAvailable() {
		return roomNumAvailable;
	}
	public void setRoomNumAvailable(int roomNumAvailable) {
		this.roomNumAvailable = roomNumAvailable;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	

}
