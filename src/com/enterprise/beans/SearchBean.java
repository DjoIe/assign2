package com.enterprise.beans;

import java.util.ArrayList;
import java.util.List;


public class SearchBean {
	private List<RoomBean> result;
	
    public SearchBean(){
        result = new ArrayList<RoomBean>();
    }


	public List<RoomBean> getResult() {
		return result;
	}

	public void setResult(List<RoomBean> result) {
		this.result = result;
	}
	
	
	
}
