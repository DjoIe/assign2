package com.enterprise.beans;

import java.util.ArrayList;

public class BookingSearchBean {
	
	private ArrayList<BookingsBeans> resBooking;
	private ArrayList<BookingsBeans> editBooking;
	private ArrayList<BookingsBeans> updatedBooking;
	
	public BookingSearchBean() {
		resBooking = new ArrayList<BookingsBeans>();
		editBooking = new ArrayList<BookingsBeans>();
		updatedBooking = new ArrayList<BookingsBeans>();
	}
	
	public ArrayList<BookingsBeans> getBookingRes() {
		return resBooking;
	}
	
	public void setBookingRes(ArrayList<BookingsBeans> foo) {
		resBooking = foo;
	}
	
	public ArrayList<BookingsBeans> getBookingsToEdit() {
		return editBooking;
	}
	
	public void setBookingsToEdit(ArrayList<BookingsBeans> foo) {
		editBooking = foo;
	}
	public ArrayList<BookingsBeans> getUpdatedBookings() {
		return updatedBooking;
	}
	
	public void setUpdatedBookings(ArrayList<BookingsBeans> foo) {
		updatedBooking = foo;
	}

}
