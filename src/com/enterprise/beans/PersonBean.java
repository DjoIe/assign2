package com.enterprise.beans;

import java.io.Serializable;
import java.util.Date;

public class PersonBean implements Serializable  {
	private String userName, nickName, firstName, lastName, phone, email, address, creditCard, password;
	
	private int id, profileId;
	private int role;
	
	Date checkInDate;
	Date checkOutDate;
	int roomNum;
	String roomType;
	int bid;//BookingId
	HotelBean hotel;//Hotel owned/managed by this person if applicable
	ChainBean chain;
	
	public ChainBean getChainBean(){
		return chain;
	}
	public void setChain(ChainBean c){
		chain = c;
	}	
	
	
	public Date getCheckInDate() {
		return checkInDate;
	}
	public Date getCheckOutDate() {
		return checkOutDate;
	}
	public int getRoomNum(){
		return roomNum;
	}
	public int getBid(){
		return bid;
	}
	public HotelBean getHotel(){
		return hotel;
	}
	public void setBid(int bid){
		this.bid = bid;
	}
	public void setHotel(HotelBean h){
		hotel = h;
	}
	public String getRoomType(){
		return roomType;
	}
	public void setCheckInDate(Date d){
		checkInDate = d;
	}
	public void setCheckOutDate(Date d){
		checkOutDate = d;
	}
	public void setRoomNum(int roomNum){
		this.roomNum = roomNum;
	}
	public void setRoomType(String roomType){
		this.roomType = roomType;
	}
	
	//Copy data from another personBean to this personBean
	public void copyDataFrom(PersonBean person){
		this.userName = person.getUserName();
		this.nickName = person.getNickName();
		this.firstName = person.getFirstName();
		this.lastName = person.getLastName();
		this.phone = person.getPhone();
		this.email = person.getEmail();
		this.address = person.getAddress();
		this.creditCard = person.getCreditCard();
		this.password = person.getPassword();
		this.id = person.getId();
		//this.profileId = person.getProfileId();
		this.role = person.getRole();
	}
	public void personBean(){
		userName = "";
		nickName = "";
		firstName = "";
		lastName = "";
		email = "";
		address = "";
		creditCard = "";
		password = "";
		phone = "";
		role = 0;
	}
	public void setRole(int role){
		this.role = role;
	}
	public void setProfileId(int profile){
		this.profileId = profile;
	}
	public void setPhone(String phone){
		this.phone = phone;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setNickName(String nickName){
		this.nickName = nickName;
	}
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	public void setEmail(String email){
		this.email = email;
	}
	public void setAddress(String address){
		this.address = address;
	}
	public void setCreditCard(String creditCard){
		this.creditCard = creditCard;
	}
	public void setId(int id){
		this.id = id;
	}
	
	public int getRole(){
		return role;
	}
	public String getPassword(){
		return password;
	}
	public String getUserName(){
		return userName;
	}
	public String getNickName(){
		return nickName;
	}
	public String getFirstName(){
		return firstName;
	}
	public String getLastName(){
		return lastName;
	}
	public String getEmail(){
		return email;
	}
	public String getAddress(){
		return address;
	}
	public String getCreditCard(){
		return creditCard;
	}
	public String getPhone(){
		return phone;
	}
	public int getProfileId(){
		return profileId;
	}
	public int getId(){
		return id;
	}
}
