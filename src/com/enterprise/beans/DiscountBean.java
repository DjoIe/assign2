package com.enterprise.beans;

import java.io.Serializable;
import java.util.Date;

public class DiscountBean implements Serializable  {
	Date startDate, endDate;
	int id, value, hid;
	String hotel, roomType;
	
	public Date getStartDate(){
		return startDate;
	}
	public Date getEndDate(){
		return endDate;
	}
	public int getId(){
		return id;
	}
	public int getValue(){
		return value;
	}
	public int getHid(){
		return hid;
	}
	public String getHotel(){
		return hotel;
	}
	public String getRoomType(){
		return roomType;
	}
	
	public void setStartDate(Date d){
		startDate = d;
	}
	public void setEndDate(Date d){
		endDate = d;
	}
	public void setId(int id){
		this.id = id;
	}
	public void setHid(int id){
		hid = id;
	}
	public void setValue(int v){
		value = v;
	}
	public void setHotel(String s){
		hotel = s;
	}
	public void setRoomType(String s){
		roomType = s;
	}
	
	public DiscountBean(){
		
	}
}
